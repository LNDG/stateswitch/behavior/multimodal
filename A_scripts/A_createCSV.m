% create a table with all relevant data
% 1 table per condition and condition difference

StateSwitchDynIDs = {1117;1118;1120;1124;1125;1126;1131;1132;1135;1136;1138;1144;1151;...
    1158;1160;1163;1164;1167;1169;1172;1173;1178;1182;1213;1214;1215;1216;...
    1219;1221;1223;1227;1228;1233;1234;1237;1239;1240;1243;1245;1247;1250;...
    1252;1257;1258;1261;1265;1266;1268;1270;1276;1281;2104;2107;2108;2112;...
    2118;2120;2121;2123;2124;2125;2129;2130;2131;2132;2133;2134;2135;2139;...
    2140;2142;2145;2147;2149;2157;2160;2201;2202;2203;2205;2206;2209;2210;...
    2211;2213;2214;2215;2216;2217;2219;2222;2224;2226;2227;2236;2237;2238;...
    2241;2244;2246;2248;2250;2251;2252;2253;2254;2255;2258;2261};

StateSwitchDynIDs = cellfun(@num2str, StateSwitchDynIDs, 'un', 0);

STSWD_summary.IDs = StateSwitchDynIDs;

idx_YA = cellfun(@str2num, StateSwitchDynIDs)<2000;
idx_OA = cellfun(@str2num, StateSwitchDynIDs)>2000;

addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S4D_MSE_CSD_v4/T_tools/winsor') % add function to winsorize data

%% add EEG values: 
% cue alpha, prestim alpha, stim alpha, probe theta, probe alpha (whole epoch baseline)
% extract from significant clusters

pn.dataTFR = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2_TFR/';

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S4B_MSE_CSD/B_data/E_statistics/K3_YA.mat', 'stat', 'cfgStat');
thetaChans = find(stat{3,5}.posclusterslabelmat);
alphaChans = 44:60;

for indID = 1:numel(StateSwitchDynIDs)
    tmp_dataFile = [pn.dataTFR, 'B_data/D3_TFRavg/', StateSwitchDynIDs{indID}, '_dynamic_TFRwavePow.mat'];
    if exist(tmp_dataFile)
        tmp = load(tmp_dataFile, 'Dim*');
        tmp_time = tmp.Dim1DataTFR_WTBL.time;
        tmp_cueTime = find(tmp_time>0 & tmp_time<1);
        tmp_prestimTime = find(tmp_time>2 & tmp_time<3);
        tmp_stimTime = find(tmp_time>3.5 & tmp_time<6);
        tmp_probeTime = find(tmp_time>6.1 & tmp_time<7);
        tmp_freq = tmp.Dim1DataTFR_WTBL.freq;
        tmp_thetaIdx = find(tmp_freq>3 & tmp_freq<7);
        tmp_alphaIdx = find(tmp_freq>8 & tmp_freq<12);
        
        probeTheta(indID,1) = squeeze(nanmean(nanmean(nanmean(tmp.Dim1DataTFR_WTBL.powspctrm(thetaChans, tmp_thetaIdx, tmp_probeTime),3),2),1));
        probeTheta(indID,2) = squeeze(nanmean(nanmean(nanmean(tmp.Dim2DataTFR_WTBL.powspctrm(thetaChans, tmp_thetaIdx, tmp_probeTime),3),2),1));
        probeTheta(indID,3) = squeeze(nanmean(nanmean(nanmean(tmp.Dim3DataTFR_WTBL.powspctrm(thetaChans, tmp_thetaIdx, tmp_probeTime),3),2),1));
        probeTheta(indID,4) = squeeze(nanmean(nanmean(nanmean(tmp.Dim4DataTFR_WTBL.powspctrm(thetaChans, tmp_thetaIdx, tmp_probeTime),3),2),1));
        
        cueAlpha(indID,1) = squeeze(nanmean(nanmean(nanmean(tmp.Dim1DataTFR_WTBL.powspctrm(alphaChans, tmp_alphaIdx, tmp_cueTime),3),2),1));
        cueAlpha(indID,2) = squeeze(nanmean(nanmean(nanmean(tmp.Dim2DataTFR_WTBL.powspctrm(alphaChans, tmp_alphaIdx, tmp_cueTime),3),2),1));
        cueAlpha(indID,3) = squeeze(nanmean(nanmean(nanmean(tmp.Dim3DataTFR_WTBL.powspctrm(alphaChans, tmp_alphaIdx, tmp_cueTime),3),2),1));
        cueAlpha(indID,4) = squeeze(nanmean(nanmean(nanmean(tmp.Dim4DataTFR_WTBL.powspctrm(alphaChans, tmp_alphaIdx, tmp_cueTime),3),2),1));
        
        prestimAlpha(indID,1) = squeeze(nanmean(nanmean(nanmean(tmp.Dim1DataTFR_WTBL.powspctrm(alphaChans, tmp_alphaIdx, tmp_prestimTime),3),2),1));
        prestimAlpha(indID,2) = squeeze(nanmean(nanmean(nanmean(tmp.Dim2DataTFR_WTBL.powspctrm(alphaChans, tmp_alphaIdx, tmp_prestimTime),3),2),1));
        prestimAlpha(indID,3) = squeeze(nanmean(nanmean(nanmean(tmp.Dim3DataTFR_WTBL.powspctrm(alphaChans, tmp_alphaIdx, tmp_prestimTime),3),2),1));
        prestimAlpha(indID,4) = squeeze(nanmean(nanmean(nanmean(tmp.Dim4DataTFR_WTBL.powspctrm(alphaChans, tmp_alphaIdx, tmp_prestimTime),3),2),1));
        
        stimAlpha(indID,1) = squeeze(nanmean(nanmean(nanmean(tmp.Dim1DataTFR_WTBL.powspctrm(alphaChans, tmp_alphaIdx, tmp_stimTime),3),2),1));
        stimAlpha(indID,2) = squeeze(nanmean(nanmean(nanmean(tmp.Dim2DataTFR_WTBL.powspctrm(alphaChans, tmp_alphaIdx, tmp_stimTime),3),2),1));
        stimAlpha(indID,3) = squeeze(nanmean(nanmean(nanmean(tmp.Dim3DataTFR_WTBL.powspctrm(alphaChans, tmp_alphaIdx, tmp_stimTime),3),2),1));
        stimAlpha(indID,4) = squeeze(nanmean(nanmean(nanmean(tmp.Dim4DataTFR_WTBL.powspctrm(alphaChans, tmp_alphaIdx, tmp_stimTime),3),2),1));
        
        probeAlpha(indID,1) = squeeze(nanmean(nanmean(nanmean(tmp.Dim1DataTFR_WTBL.powspctrm(alphaChans, tmp_alphaIdx, tmp_probeTime),3),2),1));
        probeAlpha(indID,2) = squeeze(nanmean(nanmean(nanmean(tmp.Dim2DataTFR_WTBL.powspctrm(alphaChans, tmp_alphaIdx, tmp_probeTime),3),2),1));
        probeAlpha(indID,3) = squeeze(nanmean(nanmean(nanmean(tmp.Dim3DataTFR_WTBL.powspctrm(alphaChans, tmp_alphaIdx, tmp_probeTime),3),2),1));
        probeAlpha(indID,4) = squeeze(nanmean(nanmean(nanmean(tmp.Dim4DataTFR_WTBL.powspctrm(alphaChans, tmp_alphaIdx, tmp_probeTime),3),2),1));
        
    else
        cueAlpha(indID,1) = NaN; cueAlpha(indID,2) = NaN; cueAlpha(indID,3) = NaN; cueAlpha(indID,4) = NaN;
        prestimAlpha(indID,1) = NaN; prestimAlpha(indID,2) = NaN; prestimAlpha(indID,3) = NaN; prestimAlpha(indID,4) = NaN;
        stimAlpha(indID,1) = NaN; stimAlpha(indID,2) = NaN; stimAlpha(indID,3) = NaN; stimAlpha(indID,4) = NaN;
        probeAlpha(indID,1) = NaN; probeAlpha(indID,2) = NaN; probeAlpha(indID,3) = NaN; probeAlpha(indID,4) = NaN;
        probeTheta(indID,1) = NaN; probeTheta(indID,2) = NaN; probeTheta(indID,3) = NaN; probeTheta(indID,4) = NaN;
    end
end

STSWD_summary.TFR.cueAlpha = cueAlpha;
STSWD_summary.TFR.prestimAlpha = prestimAlpha;
STSWD_summary.TFR.stimAlpha = stimAlpha;
STSWD_summary.TFR.probeAlpha = probeAlpha;
STSWD_summary.TFR.probeTheta = probeTheta;

%% get first level z-values for parametric effect (no BL)

for indID = 1:numel(StateSwitchDynIDs)
    tmp_dataFile = [pn.dataTFR, 'B_data/D3_stats/', StateSwitchDynIDs{indID}, '_Stat1stLevel_noBL.mat'];
    if exist(tmp_dataFile)
        tmp = load(tmp_dataFile, 'stat');
        tmp_time = tmp.stat.time;
        tmp_cueTime = find(tmp_time>0 & tmp_time<1);
        tmp_prestimTime = find(tmp_time>2 & tmp_time<3);
        tmp_stimTime = find(tmp_time>3.5 & tmp_time<6);
        tmp_probeTime = find(tmp_time>6.1 & tmp_time<7);
        tmp_freq = tmp.stat.freq;
        tmp_thetaIdx = find(tmp_freq>3 & tmp_freq<7);
        tmp_alphaIdx = find(tmp_freq>8 & tmp_freq<12);
        
        probeTheta(indID,1) = squeeze(nanmean(nanmean(nanmean(tmp.stat.stat(thetaChans, tmp_thetaIdx, tmp_probeTime),3),2),1));
        cueAlpha(indID,1) = squeeze(nanmean(nanmean(nanmean(tmp.stat.stat(alphaChans, tmp_alphaIdx, tmp_cueTime),3),2),1));
        prestimAlpha(indID,1) = squeeze(nanmean(nanmean(nanmean(tmp.stat.stat(alphaChans, tmp_alphaIdx, tmp_prestimTime),3),2),1));
        stimAlpha(indID,1) = squeeze(nanmean(nanmean(nanmean(tmp.stat.stat(alphaChans, tmp_alphaIdx, tmp_stimTime),3),2),1));
        probeAlpha(indID,1) = squeeze(nanmean(nanmean(nanmean(tmp.stat.stat(alphaChans, tmp_alphaIdx, tmp_probeTime),3),2),1));
    else
        cueAlpha(indID,1) = NaN;
        prestimAlpha(indID,1) = NaN;
        stimAlpha(indID,1) = NaN;
        probeAlpha(indID,1) = NaN;
        probeTheta(indID,1) = NaN;
    end
end

STSWD_summary.TFR_1stLevelStat_noBL.cueAlpha = cueAlpha;
STSWD_summary.TFR_1stLevelStat_noBL.prestimAlpha = prestimAlpha;
STSWD_summary.TFR_1stLevelStat_noBL.stimAlpha = stimAlpha;
STSWD_summary.TFR_1stLevelStat_noBL.probeAlpha = probeAlpha;
STSWD_summary.TFR_1stLevelStat_noBL.probeTheta = probeTheta;

%% get first level z-values for parametric effect (WTBL)

for indID = 1:numel(StateSwitchDynIDs)
    tmp_dataFile = [pn.dataTFR, 'B_data/D3_stats/', StateSwitchDynIDs{indID}, '_Stat1stLevel_WTBL.mat'];
    if exist(tmp_dataFile)
        tmp = load(tmp_dataFile, 'stat');
        tmp_time = tmp.stat.time;
        tmp_cueTime = find(tmp_time>0 & tmp_time<1);
        tmp_prestimTime = find(tmp_time>2 & tmp_time<3);
        tmp_stimTime = find(tmp_time>3.5 & tmp_time<6);
        tmp_probeTime = find(tmp_time>6.1 & tmp_time<7);
        tmp_freq = tmp.stat.freq;
        tmp_thetaIdx = find(tmp_freq>3 & tmp_freq<7);
        tmp_alphaIdx = find(tmp_freq>8 & tmp_freq<12);
        
        probeTheta(indID,1) = squeeze(nanmean(nanmean(nanmean(tmp.stat.stat(thetaChans, tmp_thetaIdx, tmp_probeTime),3),2),1));
        cueAlpha(indID,1) = squeeze(nanmean(nanmean(nanmean(tmp.stat.stat(alphaChans, tmp_alphaIdx, tmp_cueTime),3),2),1));
        prestimAlpha(indID,1) = squeeze(nanmean(nanmean(nanmean(tmp.stat.stat(alphaChans, tmp_alphaIdx, tmp_prestimTime),3),2),1));
        stimAlpha(indID,1) = squeeze(nanmean(nanmean(nanmean(tmp.stat.stat(alphaChans, tmp_alphaIdx, tmp_stimTime),3),2),1));
        probeAlpha(indID,1) = squeeze(nanmean(nanmean(nanmean(tmp.stat.stat(alphaChans, tmp_alphaIdx, tmp_probeTime),3),2),1));
    else
        cueAlpha(indID,1) = NaN;
        prestimAlpha(indID,1) = NaN;
        stimAlpha(indID,1) = NaN;
        probeAlpha(indID,1) = NaN;
        probeTheta(indID,1) = NaN;
    end
end

STSWD_summary.TFR_1stLevelStat_WTBL.cueAlpha = cueAlpha;
STSWD_summary.TFR_1stLevelStat_WTBL.prestimAlpha = prestimAlpha;
STSWD_summary.TFR_1stLevelStat_WTBL.stimAlpha = stimAlpha;
STSWD_summary.TFR_1stLevelStat_WTBL.probeAlpha = probeAlpha;
STSWD_summary.TFR_1stLevelStat_WTBL.probeTheta = probeTheta;

%% SSVEP (30 Hz) + stim gamma (MTM)

%% brainscores LV1 LV2 LV3 LV5

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/B_data/B_brainscoreSummary.mat', 'brainscoreSummary')

% align to STSWD subject structure

tmp_IDidx = ismember(StateSwitchDynIDs, brainscoreSummary.IDs);
STSWD_summary.brainscoreSummary.meanTask(tmp_IDidx,:) = brainscoreSummary.meanTask;
STSWD_summary.brainscoreSummary.DDM_LV1(tmp_IDidx,:) = brainscoreSummary.DDM_LV1;
STSWD_summary.brainscoreSummary.DDM_LV2(tmp_IDidx,:) = brainscoreSummary.DDM_LV2;
STSWD_summary.brainscoreSummary.DDM_LV3(tmp_IDidx,:) = brainscoreSummary.DDM_LV3;
STSWD_summary.brainscoreSummary.DDM_LV5(tmp_IDidx,:) = brainscoreSummary.DDM_LV5;
STSWD_summary.brainscoreSummary.meanTaskv2(tmp_IDidx,:) = brainscoreSummary.meanTaskv2;
STSWD_summary.brainscoreSummary.SDTaskv2(tmp_IDidx,:) = brainscoreSummary.SDTaskv2;

clear tmp_* brainscoreSummary

%% median RT, mean Acc (session-specific + PCA across sessions)

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/A_MergeIndividualData/B_data/SummaryData_N102.mat')

tmp_IDidx = ismember(StateSwitchDynIDs, IDs_all);

STSWD_summary.behav.EEGAcc(tmp_IDidx,:) = squeeze(nanmean(SummaryData.EEG.Acc_mean,2));
STSWD_summary.behav.EEGRT(tmp_IDidx,:) = squeeze(nanmean(SummaryData.EEG.RTs_md,2));
STSWD_summary.behav.MRIAcc(tmp_IDidx,:) = squeeze(nanmean(SummaryData.MRI.Acc_mean,2));
STSWD_summary.behav.MRIRT(tmp_IDidx,:) = squeeze(nanmean(SummaryData.MRI.RTs_md,2));

% calculate PCA across sessions 
for indCond = 1:4
    [coeff, score] = pca([STSWD_summary.behav.MRIRT(:,indCond), STSWD_summary.behav.EEGRT(:,indCond)]);
    STSWD_summary.behav.RT_PCA(:,indCond) = score(:,1); % get first principle component across sessions
    %figure; scatter(STSWD_summary.behav.MRIRT(:,indCond), score(:,1))
    [coeff, score] = pca([STSWD_summary.behav.MRIAcc(:,indCond), STSWD_summary.behav.EEGAcc(:,indCond)]);
    STSWD_summary.behav.Acc_PCA(:,indCond) = score(:,1); % get first principle component across sessions
end

% linear change in brainscore
X = [1 1; 1 2; 1 3; 1 4];
b=X\STSWD_summary.behav.EEGAcc'; STSWD_summary.behav.EEGAcc_linear(:,1) = b(2,:);
b=X\STSWD_summary.behav.EEGRT'; STSWD_summary.behav.EEGRT_linear(:,1) = b(2,:);
b=X\STSWD_summary.behav.MRIAcc'; STSWD_summary.behav.MRIAcc_linear(:,1) = b(2,:);
b=X\STSWD_summary.behav.MRIRT'; STSWD_summary.behav.MRIRT_linear(:,1) = b(2,:);

clear tmp_* SummaryData

%% DDM (session-specific + PCA across sessions)

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/D_DDM/B_data/DDM_summary.mat')

tmp_IDidx = ismember(StateSwitchDynIDs, DDM_summary.IDs);

STSWD_summary.DDM.thresholdMRI(tmp_IDidx,:) = DDM_summary.thresholdMRI;
STSWD_summary.DDM.nondecisionMRI(tmp_IDidx,:) = DDM_summary.nondecisionMRI;
STSWD_summary.DDM.driftMRI(tmp_IDidx,:) = DDM_summary.driftMRI;
STSWD_summary.DDM.thresholdEEG(tmp_IDidx,:) = DDM_summary.thresholdEEG;
STSWD_summary.DDM.nondecisionEEG(tmp_IDidx,:) = DDM_summary.nondecisionEEG;
STSWD_summary.DDM.driftEEG(tmp_IDidx,:) = DDM_summary.driftEEG;

% plot reliability
% figure; 
% subplot(1,3,1); imagesc(corrcoef([STSWD_summary.DDM.thresholdMRI, STSWD_summary.DDM.thresholdEEG], 'rows', 'complete'), [-1 1])
% subplot(1,3,2); imagesc(corrcoef([STSWD_summary.DDM.nondecisionMRI, STSWD_summary.DDM.nondecisionEEG], 'rows', 'complete'), [-1 1])
% subplot(1,3,3); imagesc(corrcoef([STSWD_summary.DDM.driftMRI, STSWD_summary.DDM.driftEEG], 'rows', 'complete'), [-1 1])

% calculate PCA across sessions 
for indCond = 1:4
    [coeff, score] = pca([STSWD_summary.DDM.thresholdMRI(:,indCond), STSWD_summary.DDM.thresholdEEG(:,indCond)]);
    STSWD_summary.DDM.threshold_PCA(:,indCond) = score(:,1); % get first principle component across sessions
    %figure; scatter(STSWD_summary.DDM.thresholdMRI(:,indCond), STSWD_summary.DDM.threshold_PCA(:,indCond))
    [coeff, score] = pca([STSWD_summary.DDM.nondecisionMRI(:,indCond), STSWD_summary.DDM.nondecisionEEG(:,indCond)]);
    STSWD_summary.DDM.nondecision_PCA(:,indCond) = score(:,1); 
    [coeff, score] = pca([STSWD_summary.DDM.driftMRI(:,indCond), STSWD_summary.DDM.driftEEG(:,indCond)]);
    STSWD_summary.DDM.drift_PCA(:,indCond) = score(:,1);
end

clear tmp_* DDM_summary

%% HDDM

% load(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/D_DDM/B_data/HDDM_summary.mat'], 'HDDM_summary')
% 
% tmp_IDidx = ismember(StateSwitchDynIDs, HDDM_summary.IDs);
% 
% STSWD_summary.HDDM.thresholdMRI(tmp_IDidx,:) = HDDM_summary.thresholdMRI;
% STSWD_summary.HDDM.nondecisionMRI(tmp_IDidx,:) = HDDM_summary.nondecisionMRI;
% STSWD_summary.HDDM.driftMRI(tmp_IDidx,:) = HDDM_summary.driftMRI;
% STSWD_summary.HDDM.thresholdEEG(tmp_IDidx,:) = HDDM_summary.thresholdEEG;
% STSWD_summary.HDDM.nondecisionEEG(tmp_IDidx,:) = HDDM_summary.nondecisionEEG;
% STSWD_summary.HDDM.driftEEG(tmp_IDidx,:) = HDDM_summary.driftEEG;

%% HDDM (including session)

% load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/D_DDM/B_data/HDDM_wSession_summary.mat')
% 
% tmp_IDidx = ismember(StateSwitchDynIDs, HDDM_wSession_summary.IDs);
% 
% STSWD_summary.HDDMwSess.thresholdMRI(tmp_IDidx,:) = HDDM_wSession_summary.thresholdMRI;
% STSWD_summary.HDDMwSess.nondecisionMRI(tmp_IDidx,:) = HDDM_wSession_summary.nondecisionMRI;
% STSWD_summary.HDDMwSess.driftMRI(tmp_IDidx,:) = HDDM_wSession_summary.driftMRI;
% STSWD_summary.HDDMwSess.thresholdEEG(tmp_IDidx,:) = HDDM_wSession_summary.thresholdEEG;
% STSWD_summary.HDDMwSess.nondecisionEEG(tmp_IDidx,:) = HDDM_wSession_summary.nondecisionEEG;
% STSWD_summary.HDDMwSess.driftEEG(tmp_IDidx,:) = HDDM_wSession_summary.driftEEG;

%% HDDM (fixed threshold and NDT)

% load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/D_DDM/B_data/HDDM_summary_v12_a2_t2.mat')
% 
% tmp_IDidx = ismember(StateSwitchDynIDs, HDDM_summary_v12_a2_t2.IDs);
% 
% STSWD_summary.HDDM_ATfixed.thresholdMRI(tmp_IDidx,:) = HDDM_summary_v12_a2_t2.thresholdMRI;
% STSWD_summary.HDDM_ATfixed.nondecisionMRI(tmp_IDidx,:) = HDDM_summary_v12_a2_t2.nondecisionMRI;
% STSWD_summary.HDDM_ATfixed.driftMRI(tmp_IDidx,:) = HDDM_summary_v12_a2_t2.driftMRI;
% STSWD_summary.HDDM_ATfixed.thresholdEEG(tmp_IDidx,:) = HDDM_summary_v12_a2_t2.thresholdEEG;
% STSWD_summary.HDDM_ATfixed.nondecisionEEG(tmp_IDidx,:) = HDDM_summary_v12_a2_t2.nondecisionEEG;
% STSWD_summary.HDDM_ATfixed.driftEEG(tmp_IDidx,:) = HDDM_summary_v12_a2_t2.driftEEG;

%% HDDM (only v&t)

% encode YAs
load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/D_DDM/B_data/HDDM_summary_YA_vt.mat');

tmp_IDidx = ismember(StateSwitchDynIDs, HDDM_summary.IDs(1:49));
tmp_IDidx_ddm = ismember(HDDM_summary.IDs, StateSwitchDynIDs(tmp_IDidx));

STSWD_summary.HDDM_vt = [];
STSWD_summary.HDDM_vt.thresholdMRI(tmp_IDidx,1:4) = HDDM_summary.thresholdMRI(tmp_IDidx_ddm,:);
STSWD_summary.HDDM_vt.nondecisionMRI(tmp_IDidx,1:4) = HDDM_summary.nondecisionMRI(tmp_IDidx_ddm,:);
STSWD_summary.HDDM_vt.driftMRI(tmp_IDidx,1:4) = HDDM_summary.driftMRI(tmp_IDidx_ddm,:);
STSWD_summary.HDDM_vt.thresholdEEG(tmp_IDidx,1:4) = HDDM_summary.thresholdEEG(tmp_IDidx_ddm,:);
STSWD_summary.HDDM_vt.nondecisionEEG(tmp_IDidx,1:4) = HDDM_summary.nondecisionEEG(tmp_IDidx_ddm,:);
STSWD_summary.HDDM_vt.driftEEG(tmp_IDidx,1:4) = HDDM_summary.driftEEG(tmp_IDidx_ddm,:);

% encode OAs
load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/D_DDM/B_data/HDDM_summary_OA_vt.mat');

tmp_IDidx = ismember(StateSwitchDynIDs, HDDM_summary.IDs(50:end));
tmp_IDidx_ddm = ismember(HDDM_summary.IDs, StateSwitchDynIDs(tmp_IDidx));

STSWD_summary.HDDM_vt.thresholdMRI(tmp_IDidx,1:4) = HDDM_summary.thresholdMRI(tmp_IDidx_ddm,:);
STSWD_summary.HDDM_vt.nondecisionMRI(tmp_IDidx,1:4) = HDDM_summary.nondecisionMRI(tmp_IDidx_ddm,:);
STSWD_summary.HDDM_vt.driftMRI(tmp_IDidx,1:4) = HDDM_summary.driftMRI(tmp_IDidx_ddm,:);
STSWD_summary.HDDM_vt.thresholdEEG(tmp_IDidx,1:4) = HDDM_summary.thresholdEEG(tmp_IDidx_ddm,:);
STSWD_summary.HDDM_vt.nondecisionEEG(tmp_IDidx,1:4) = HDDM_summary.nondecisionEEG(tmp_IDidx_ddm,:);
STSWD_summary.HDDM_vt.driftEEG(tmp_IDidx,1:4) = HDDM_summary.driftEEG(tmp_IDidx_ddm,:);

% add slopes
X = [1 1; 1 2; 1 3; 1 4]; 
b=X\STSWD_summary.HDDM_vt.thresholdMRI'; STSWD_summary.HDDM_vt.thresholdMRI_linear(:,1) = b(2,:);
b=X\STSWD_summary.HDDM_vt.nondecisionMRI'; STSWD_summary.HDDM_vt.nondecisionMRI_linear(:,1) = b(2,:);
b=X\STSWD_summary.HDDM_vt.driftMRI'; STSWD_summary.HDDM_vt.driftMRI_linear(:,1) = b(2,:);
b=X\STSWD_summary.HDDM_vt.thresholdEEG'; STSWD_summary.HDDM_vt.thresholdEEG_linear(:,1) = b(2,:);
b=X\STSWD_summary.HDDM_vt.nondecisionEEG'; STSWD_summary.HDDM_vt.nondecisionEEG_linear(:,1) = b(2,:);
b=X\STSWD_summary.HDDM_vt.driftEEG'; STSWD_summary.HDDM_vt.driftEEG_linear(:,1) = b(2,:);

%% pupil dilation: cue, stim

load(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eye/SA_EEG/B_analyses/A_pupilDilation/B_data/A_pupilDiameter_cutByStim/C_summaryPupil.mat'], 'summaryPupil')

tmp_IDidx = ismember(StateSwitchDynIDs, summaryPupil.IDs);

STSWD_summary.pupil.cuePupilRaw(tmp_IDidx,:) = summaryPupil.cuePupilRaw;
STSWD_summary.pupil.stimPupilRaw(tmp_IDidx,:) = summaryPupil.stimPupilRaw;
STSWD_summary.pupil.cuePupilZ(tmp_IDidx,:) = summaryPupil.cuePupilZ;
STSWD_summary.pupil.stimPupilZ(tmp_IDidx,:) = summaryPupil.stimPupilZ;
STSWD_summary.pupil.cuePupilRelChange(tmp_IDidx,:) = summaryPupil.cuePupilRelChange;
STSWD_summary.pupil.stimPupilRelChange(tmp_IDidx,:) = summaryPupil.stimPupilRelChange;

%% novel pupil Definition: stim only

pn.pupil = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eye/SA_EEG/B_analyses/A_pupilDilation/B_data/A_pupilDiameter_cutByStim/';
load([pn.pupil, 'G1_summaryPupil.mat'], 'summaryPupil')
tmp_IDidx = ismember(StateSwitchDynIDs, summaryPupil.IDs);

STSWD_summary.pupil2.stimdiff(tmp_IDidx,:) = summaryPupil.stimPupilDeriv;
STSWD_summary.pupil2.stimdiff_slope(tmp_IDidx,1) = summaryPupil.stimPupilDeriv_slopes;

load([pn.pupil, 'G2_summaryPupil_raw.mat'], 'summaryPupil')

STSWD_summary.pupil2.stimraw(tmp_IDidx,:) = summaryPupil.stimRaw;
STSWD_summary.pupil2.stimraw_slope(tmp_IDidx,1) = summaryPupil.stimRaw_slopes;

load([pn.pupil, 'G3_summaryPupil_bl.mat'], 'summaryPupil')

STSWD_summary.pupil2.stimbl(tmp_IDidx,:) = summaryPupil.stimbl;
STSWD_summary.pupil2.stimbl_slope(tmp_IDidx,1) = summaryPupil.stimbl_slopes;

%% 1/f 

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S18_1_f/B_data/C_SlopeFits_v3.mat', 'SlopeFits')
load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S18_1_f/B_data/C_stat_v3.mat', 'stat')

tmp_IDidx = ismember(StateSwitchDynIDs, SlopeFits.IDs);

STSWD_summary.OneFslope.data(tmp_IDidx,:) = squeeze(nanmean(SlopeFits.linFit_2_30(:,:,find(stat.posclusterslabelmat==2)),3));

% linear change in 1/f

X = [1 1; 1 2; 1 3; 1 4];
b=X\squeeze(nanmean(SlopeFits.linFit_2_30(:,:,find(stat.posclusterslabelmat==2)),3))';
STSWD_summary.OneFslope.linear(tmp_IDidx,1) = b(2,:);

% winsorize change values

STSWD_summary.OneFslope.linear_win = winsor(STSWD_summary.OneFslope.linear, [0 95]);

figure; scatter(STSWD_summary.OneFslope.linear_win(tmp_IDidx,1),STSWD_summary.OneFslope.linear(tmp_IDidx), 'filled')

% figure; scatter(STSWD_summary.OneFslope.linear_win(tmp_IDidx,1),STSWD_summary.HDDM_vt.driftEEG(tmp_IDidx), 'filled')
% figure; scatter(STSWD_summary.OneFslope.linear_win(tmp_IDidx,1),STSWD_summary.pupil2.stimraw_slope(tmp_IDidx,1), 'filled')
% figure; scatter(STSWD_summary.OneFslope.linear_win(tmp_IDidx,1),STSWD_summary.EEG_LV1.slope(tmp_IDidx,1), 'filled')

%% fine-scale entropy

load(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S4D_MSE_CSD_v4/B_data/E_statistics/L0_linearTest_individualdata.mat'], 'individualMSE');
tmp_IDidx = ismember(StateSwitchDynIDs, individualMSE.IDs);
STSWD_summary.SE.data(tmp_IDidx,:) = individualMSE.data;
STSWD_summary.SE.data_slope(tmp_IDidx,1) = individualMSE.data_slope;
STSWD_summary.SE.data_slope_win(tmp_IDidx,1) = individualMSE.data_slope_win;

%figure; scatter(STSWD_summary.pupil2.stimraw_slope(tmp_IDidx,1),STSWD_summary.MSE.data_slope_win(tmp_IDidx), 'filled')

% attempt to create a common LV

% [lv, coeff] = pca([zscore(STSWD_summary.MSE_slope(tmp_IDidx,1)), zscore(STSWD_summary.EEG_LV1.slope(tmp_IDidx,1)), ...
%     zscore(STSWD_summary.spectralslope_linearChange(tmp_IDidx,1)),zscore(STSWD_summary.pupil2.stimraw_slope(tmp_IDidx,1)),...
%     zscore(STSWD_summary.HDDM_vt.driftEEG(tmp_IDidx))]);
% 
% figure; imagesc(lv, [-1 1])
% figure; imagesc(coeff)
 
%% add EEG-based brainscore

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2B_TFR_v6/B_data/M2_mencemtPLS_v6_wGamma_BS.mat', 'BS_meancent')
tmp_IDidx = ismember(StateSwitchDynIDs, BS_meancent.IDs);

STSWD_summary.EEG_LV1.data(tmp_IDidx,:) = BS_meancent.data';
% linear change in brainscore
X = [1 1; 1 2; 1 3; 1 4];
b=X\BS_meancent.data;
STSWD_summary.EEG_LV1.slope(tmp_IDidx,1) = b(2,:);

addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S4D_MSE_CSD_v4/T_tools/winsor');

STSWD_summary.EEG_LV1.slope_win = winsor(STSWD_summary.EEG_LV1.slope,[0 95]);

% figure; scatter(STSWD_summary.EEG_LV1.slope(tmp_IDidx,1),STSWD_summary.HDDM_vt.driftEEG(tmp_IDidx), 'filled')
% figure; scatter(STSWD_summary.EEG_LV1.slope(tmp_IDidx,1),squeeze(nanmean(STSWD_summary.spectralslope_linearChange(tmp_IDidx,1),2)), 'filled')

%% add prestim alpha brainscore

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2B_TFR_v6/B_data/I1B_taskPLS_prestim.mat', 'BS_meancent')
tmp_IDidx = ismember(StateSwitchDynIDs, BS_meancent.IDs);

STSWD_summary.EEG_prestim.data(tmp_IDidx,:) = BS_meancent.data';
% linear change in brainscore
X = [1 1; 1 2; 1 3; 1 4];
b=X\BS_meancent.data;
STSWD_summary.EEG_prestim.slope(tmp_IDidx,1) = b(2,:);

%% add probe-related task PLS brainscore

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2B_TFR_v6/B_data/J1B_taskPLS_probe.mat', 'BS_meancent')
tmp_IDidx = ismember(StateSwitchDynIDs, BS_meancent.IDs);

STSWD_summary.EEG_probe.data(tmp_IDidx,:) = BS_meancent.data';
% linear change in brainscore
X = [1 1; 1 2; 1 3; 1 4];
b=X\BS_meancent.data;
STSWD_summary.EEG_probe.slope(tmp_IDidx,1) = b(2,:);

STSWD_summary.EEG_probe.slope_win = winsor(STSWD_summary.EEG_probe.slope,[0 95]);

%% add CPP slope

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/B_data/Z_CPPslopes_YA.mat', 'CPPslopes')
tmp_IDidx = ismember(StateSwitchDynIDs, CPPslopes.IDs);

STSWD_summary.CPPSlope.data(tmp_IDidx,:) = CPPslopes.data;
% linear change in brainscore
X = [1 1; 1 2; 1 3; 1 4];
b=X\CPPslopes.data';
STSWD_summary.CPPSlope.slope(tmp_IDidx,1) = b(2,:);

%% add CPP threshold

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/B_data/Z_CPPthreshold_YA.mat', 'CPPthreshold')
tmp_IDidx = ismember(StateSwitchDynIDs, CPPthreshold.IDs);

STSWD_summary.CPPthreshold.data(tmp_IDidx,:) = CPPthreshold.data;
% linear change in brainscore
X = [1 1; 1 2; 1 3; 1 4];
b=X\CPPthreshold.data';
STSWD_summary.CPPthreshold.slope(tmp_IDidx,1) = b(2,:);

%% add beta slope

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2_TFR/B_data/Z_BetaSlope_YA.mat', 'BetaSlope')
tmp_IDidx = ismember(StateSwitchDynIDs, BetaSlope.IDs);

STSWD_summary.BetaSlope.data(tmp_IDidx,:) = BetaSlope.data;
% linear change in brainscore
X = [1 1; 1 2; 1 3; 1 4];
b=X\BetaSlope.data';
STSWD_summary.BetaSlope.slope(tmp_IDidx,1) = b(2,:);

%% add beta threshold

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2_TFR/B_data/Z_BetaThreshold_YA.mat', 'BetaThreshold')
tmp_IDidx = ismember(StateSwitchDynIDs, BetaThreshold.IDs);

STSWD_summary.BetaThreshold.data(tmp_IDidx,:) = BetaThreshold.data;
% linear change in brainscore
X = [1 1; 1 2; 1 3; 1 4];
b=X\BetaThreshold.data';
STSWD_summary.BetaThreshold.slope(tmp_IDidx,1) = b(2,:);

%% add frontal NDT (?) potential

load(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/B_data/Z_NDTpotential_YA.mat'], 'NDTpotential');

tmp_IDidx = ismember(StateSwitchDynIDs, NDTpotential.IDs);

STSWD_summary.NDTpotential.data(tmp_IDidx,:) = NDTpotential.data;
% linear change in brainscore
X = [1 1; 1 2; 1 3; 1 4];
b=X\NDTpotential.data';
STSWD_summary.NDTpotential.slope(tmp_IDidx,1) = b(2,:);

%% add response-related theta power

load(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2_TFR/B_data/Z_ThetaResp_YA.mat'], 'ThetaResp');

tmp_IDidx = ismember(StateSwitchDynIDs, ThetaResp.IDs);

STSWD_summary.ThetaResp.data(tmp_IDidx,:) = ThetaResp.data;
% linear change in brainscore
X = [1 1; 1 2; 1 3; 1 4];
b=X\ThetaResp.data';
STSWD_summary.ThetaResp.slope(tmp_IDidx,1) = b(2,:);

%% add spatial PCA dimensionality

% get computed dimensionality scores
    
Dimensionality_block = [];
for indID = 1:numel(StateSwitchDynIDs)
    try
        load(['/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP/B5_dimensionality/B_data/D_dimensionality/',...
            num2str(StateSwitchDynIDs{indID}),'PCAcorr_dim_spatial.mat'])
        Dimensionality_block(indID,:,:) = Dimensions_block;
    catch
        Dimensionality_block(indID,:,:) = NaN;
    end
end


STSWD_summary.PCAdim.dim = Dimensionality_block; clear Dimensionality_block;


%% add SSVEP magnitude

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S7_SSVEP/B_data/E_singleTrialFFT/Z_SSVEPmag.mat', 'SSVEPmag')

tmp_IDidx = ismember(StateSwitchDynIDs, SSVEPmag.IDs);

STSWD_summary.SSVEPmag.data_orig(tmp_IDidx,:) = SSVEPmag.orig.data;
STSWD_summary.SSVEPmag.data_norm(tmp_IDidx,:) = SSVEPmag.norm.data;

% linear change in SSVEP

X = [1 1; 1 2; 1 3; 1 4];
b=X\STSWD_summary.SSVEPmag.data_orig(tmp_IDidx,:)';
STSWD_summary.SSVEPmag.data_orig_linear(tmp_IDidx,1) = b(2,:);

b=X\STSWD_summary.SSVEPmag.data_norm(tmp_IDidx,:)';
STSWD_summary.SSVEPmag.data_norm_linear(tmp_IDidx,1) = b(2,:);

% figure; scatter(STSWD_summary.SSVEPmag.data_norm_linear(tmp_IDidx,1),STSWD_summary.SSVEPmag.data_orig_linear(tmp_IDidx), 'filled')
% figure; scatter(STSWD_summary.SSVEPmag.data_norm_linear(tmp_IDidx,1),STSWD_summary.HDDM_vt.driftEEG(tmp_IDidx), 'filled')
% figure; scatter(STSWD_summary.SSVEPmag.data_norm_linear(tmp_IDidx,1),STSWD_summary.pupil2.stimraw_slope(tmp_IDidx,1), 'filled')
% figure; scatter(STSWD_summary.SSVEPmag.data_norm_linear(tmp_IDidx,1),STSWD_summary.EEG_LV1.slope(tmp_IDidx,1), 'filled')

%% add SPM task PLS brainscore (LV1)

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B4_PLS_preproc2/B_data/SPM_STSWD_v3/meanPLS_STSWD_Mean_fullModel_N41_3mm_1000P1000B_BfMRIresult_lv1_bs.mat', 'BS')
tmp_IDidx = ismember(StateSwitchDynIDs, BS.IDs);

STSWD_summary.SPM_task.LV1.data(tmp_IDidx,:) = BS.data';
% linear change in brainscore
X = [1 1; 1 2; 1 3; 1 4];
b=X\BS.data;
STSWD_summary.SPM_task.LV1.slope(tmp_IDidx,1) = b(2,:);

%% add SPM task PLS brainscore (LV2)

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B4_PLS_preproc2/B_data/SPM_STSWD_v3/meanPLS_STSWD_Mean_fullModel_N41_3mm_1000P1000B_BfMRIresult_lv2_bs.mat', 'BS')
tmp_IDidx = ismember(StateSwitchDynIDs, BS.IDs);

STSWD_summary.SPM_task.LV2.data(tmp_IDidx,:) = BS.data';
% linear change in brainscore
X = [1 2; 1 3; 1 4];
b=X\BS.data(2:4,:);
STSWD_summary.SPM_task.LV2.slope(tmp_IDidx,1) = b(2,:);

%% add rhythm-specific indices

load('/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP_eeg/S5D_eBOSC_CSD_Stim_v2/A_scripts/C_interrelations/B_data/A_eBOSCestimates.mat')
tmp_IDidx = ismember(StateSwitchDynIDs, eBOSCestimates.IDs);

STSWD_summary.eBOSC.rhythmicAlphaPower.data(tmp_IDidx,:) = eBOSCestimates.rhythmicAlphaPower;
X = [1 1; 1 2; 1 3; 1 4];
b=X\eBOSCestimates.rhythmicAlphaPower';
STSWD_summary.eBOSC.rhythmicAlphaPower.slope(tmp_IDidx,1) = b(2,:);

STSWD_summary.eBOSC.rhythmicAlphaDuration.data(tmp_IDidx,:) = eBOSCestimates.rhythmicAlphaDuration;
X = [1 1; 1 2; 1 3; 1 4];
b=X\eBOSCestimates.rhythmicAlphaDuration';
STSWD_summary.eBOSC.rhythmicAlphaDuration.slope(tmp_IDidx,1) = b(2,:);

STSWD_summary.eBOSC.rhythmicThetaPower.data(tmp_IDidx,:) = eBOSCestimates.rhythmicThetaPower;
X = [1 1; 1 2; 1 3; 1 4];
b=X\eBOSCestimates.rhythmicThetaPower';
STSWD_summary.eBOSC.rhythmicThetaPower.slope(tmp_IDidx,1) = b(2,:);

STSWD_summary.eBOSC.rhythmicThetaDuration.data(tmp_IDidx,:) = eBOSCestimates.rhythmicThetaDuration;
X = [1 1; 1 2; 1 3; 1 4];
b=X\eBOSCestimates.rhythmicThetaDuration';
STSWD_summary.eBOSC.rhythmicThetaDuration.slope(tmp_IDidx,1) = b(2,:);

%% save IDs 

STSWD_summary.IDs = StateSwitchDynIDs;

%% save entire structure

pn.dataOut = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/B_data/';

%save([pn.dataOut, 'STSWD_summary.mat'], 'STSWD_summary')

load([pn.dataOut, 'STSWD_summary.mat'], 'STSWD_summary')
