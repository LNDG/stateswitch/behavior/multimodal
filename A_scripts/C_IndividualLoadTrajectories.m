load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/B_data/STSWD_summary.mat')


figure; imagesc(zscore(STSWD_summary.TFR.stimAlpha,[],2))

figure; bar(nanmean(zscore(STSWD_summary.TFR.stimAlpha(idx_YA,:),[],2),1))
figure; bar(nanmean(zscore(STSWD_summary.TFR.stimAlpha(idx_OA,:),[],2),1))

figure; bar(nanmean(zscore(STSWD_summary.brainscoreSummary.SDTaskv2(idx_YA,:),[],2),1))
figure; bar(nanmean(zscore(STSWD_summary.brainscoreSummary.SDTaskv2(idx_OA,:),[],2),1))


figure; bar(nanmean(STSWD_summary.TFR.prestimAlpha(idx_YA,:),1))
figure; bar(nanmean(STSWD_summary.TFR.prestimAlpha(idx_OA,:),1))

figure; bar(nanmean(STSWD_summary.pupil.stimPupilRelChange,1))
figure; bar(nanmean(STSWD_summary.HDDM.driftMRI,1))
figure; bar(nanmean(STSWD_summary.brainscoreSummary.meanTaskv2,1))
figure; bar(nanmean(STSWD_summary.brainscoreSummary.SDTaskv2,1))
figure; bar(nanmean(STSWD_summary.PCAdim.dim,1))


figure; bar(nanmean(STSWD_summary.TFR.stimAlpha,1))


figure; bar(nanmean(zscore(STSWD_summary.TFR.stimAlpha,[],2),1))

[maxVal, maxInd] = min(STSWD_summary.TFR.stimAlpha,[],2);

[maxVal, maxInd] = max(diff(STSWD_summary.TFR.stimAlpha,[],2),[],2);
[maxVal, maxInd] = max(diff(STSWD_summary.HDDM.driftMRI,[],2),[],2);

maxInd(isnan(maxVal)) = 0;


figure; histogram(maxInd)

figure; 
bar(nanmean(zscore(STSWD_summary.HDDM.driftMRI(maxInd==4,:),[],2),1))

[~, sortVal] = sort(maxInd, 'ascend');

figure; imagesc(zscore(STSWD_summary.pupil.stimPupilRelChange,[],2))
figure; imagesc(zscore(STSWD_summary.HDDM.driftEEG,[],2)-zscore(STSWD_summary.TFR.stimAlpha,[],2))
figure; imagesc(zscore(STSWD_summary.HDDM.driftEEG,[],2))

figure; imagesc(zscore(STSWD_summary.pupil.stimPupilRelChange,[],2))


figure; imagesc(zscore(STSWD_summary.pupil.stimPupilRelChange(sortVal,:),[],2))

corrcoef([zscore(STSWD_summary.pupil.stimPupilRelChange,[],2)', zscore(STSWD_summary.HDDM.driftMRI,[],2)'])
figure; imagesc(corrcoef([zscore(STSWD_summary.pupil.stimPupilRelChange,[],2)', zscore(STSWD_summary.TFR.stimAlpha,[],2)']))
figure; imagesc(corrcoef([zscore(STSWD_summary.HDDM.driftMRI,[],2)', zscore(STSWD_summary.TFR.stimAlpha,[],2)']))
figure; imagesc(corrcoef([zscore(STSWD_summary.brainscoreSummary.SDTaskv2,[],2)', zscore(STSWD_summary.TFR.stimAlpha,[],2)']))
figure; imagesc(corrcoef([STSWD_summary.HDDM.driftEEG', STSWD_summary.TFR.stimAlpha']))
figure; imagesc(corrcoef([STSWD_summary.HDDM.nondecisionEEG', STSWD_summary.TFR.stimAlpha']))
figure; imagesc(corrcoef([STSWD_summary.HDDM.thresholdEEG', STSWD_summary.TFR.stimAlpha']))

figure; hold on; 
scatter(nanmean(STSWD_summary.TFR.stimAlpha(idx_YA,:),2),nanmean(STSWD_summary.HDDM.driftEEG(idx_YA,:),2), 'filled'); lsline();
[r,p] = corrcoef(nanmean(STSWD_summary.TFR.stimAlpha(idx_YA,:),2),nanmean(STSWD_summary.HDDM.driftEEG(idx_YA,:),2))

scatter(nanmean(STSWD_summary.TFR.stimAlpha(idx_OA,:),2),nanmean(STSWD_summary.HDDM.driftEEG(idx_OA,:),2), 'filled'); lsline();

figure; hold on; 
scatter(nanmean(STSWD_summary.TFR.stimAlpha(1:47,:),2),nanmean(STSWD_summary.pupil.stimPupilRelChange(1:47,:),2), 'filled'); lsline();
scatter(nanmean(STSWD_summary.TFR.stimAlpha(48:end,:),2),nanmean(STSWD_summary.pupil.stimPupilRelChange(48:end,:),2), 'filled'); lsline();

figure; hold on;
scatter(nanmean(STSWD_summary.TFR.stimAlpha(idx_YA,4)-STSWD_summary.TFR.stimAlpha(idx_YA,1),2),nanmean(STSWD_summary.HDDM.driftEEG(idx_YA,4)-STSWD_summary.HDDM.driftEEG(idx_YA,1),2), 'filled'); lsline();
scatter(nanmean(STSWD_summary.TFR.stimAlpha(idx_OA,4)-STSWD_summary.TFR.stimAlpha(idx_OA,1),2),nanmean(STSWD_summary.HDDM.driftEEG(idx_OA,4)-STSWD_summary.HDDM.driftEEG(idx_OA,1),2), 'filled'); lsline();


figure; hold on; 
scatter(nanmean(STSWD_summary.TFR.stimAlpha(1:47,1),2),nanmean(STSWD_summary.brainscoreSummary.meanTaskv2(1:47,1),2), 'filled'); lsline();
scatter(nanmean(STSWD_summary.TFR.stimAlpha(48:end,1),2),nanmean(STSWD_summary.brainscoreSummary.meanTaskv2(48:end,1),2), 'filled'); lsline();


figure; imagesc(corrcoef([STSWD_summary.TFR.stimAlpha, STSWD_summary.brainscoreSummary.SDTaskv2(:,1:4)]))

idx_YA = find(~isnan(mean(STSWD_summary.TFR.stimAlpha(1:51,:),2)) & mean(STSWD_summary.brainscoreSummary.meanTaskv2(1:51,:),2)~=0);
idx_OA = 51+find(~isnan(mean(STSWD_summary.TFR.stimAlpha(52:end,:),2)) & mean(STSWD_summary.brainscoreSummary.meanTaskv2(52:end,:),2)~=0);

figure; hold on; 
scatter(nanmean(STSWD_summary.TFR.stimAlpha(idx_YA,:),2),nanmean(STSWD_summary.brainscoreSummary.meanTaskv2(idx_YA,:),2), 'filled'); lsline();
scatter(nanmean(STSWD_summary.TFR.stimAlpha(idx_OA,:),2),nanmean(STSWD_summary.brainscoreSummary.meanTaskv2(idx_OA,:),2), 'filled'); lsline();

figure; hold on; 
indCond = 1:4;
scatter(nanmean(STSWD_summary.TFR.stimAlpha(idx_YA,indCond),2),nanmean(STSWD_summary.brainscoreSummary.SDTaskv2(idx_YA,indCond),2), 'filled'); lsline();
[r,p] = corrcoef(nanmean(STSWD_summary.TFR.stimAlpha(idx_YA,indCond),2),nanmean(STSWD_summary.brainscoreSummary.SDTaskv2(idx_YA,indCond),2))
scatter(nanmean(STSWD_summary.TFR.stimAlpha(idx_OA,indCond),2),nanmean(STSWD_summary.brainscoreSummary.SDTaskv2(idx_OA,indCond),2), 'filled'); lsline();
[r,p] = corrcoef(nanmean(STSWD_summary.TFR.stimAlpha(idx_OA,indCond),2),nanmean(STSWD_summary.brainscoreSummary.SDTaskv2(idx_OA,indCond),2))
xlabel('Stim-related Alpha power across loads'); ylabel('SD BOLD Brainscore across loads')
title('Higher SD BOLD --> Stronger alpha desync (only YA)')
set(findall(gcf,'-property','FontSize'),'FontSize',18)



figure; hold on; 
scatter(nanmean(STSWD_summary.TFR.stimAlpha(idx_YA,2:4),2)-nanmean(STSWD_summary.TFR.stimAlpha(idx_YA,1),2),nanmean(STSWD_summary.brainscoreSummary.SDTaskv2(idx_YA,2:4),2)-nanmean(STSWD_summary.brainscoreSummary.SDTaskv2(idx_YA,1),2), 'filled'); lsline();
scatter(nanmean(STSWD_summary.TFR.stimAlpha(idx_OA,2:4),2)-nanmean(STSWD_summary.TFR.stimAlpha(idx_OA,1),2),nanmean(STSWD_summary.brainscoreSummary.SDTaskv2(idx_OA,2:4),2)-nanmean(STSWD_summary.brainscoreSummary.SDTaskv2(idx_OA,1),2), 'filled'); lsline();
[r,p] = corrcoef(nanmean(STSWD_summary.TFR.stimAlpha(idx_YA,2:4),2)-nanmean(STSWD_summary.TFR.stimAlpha(idx_YA,1),2),nanmean(STSWD_summary.brainscoreSummary.SDTaskv2(idx_YA,2:4),2)-nanmean(STSWD_summary.brainscoreSummary.SDTaskv2(idx_YA,1),2))

figure; hold on; 
scatter(nanmean(STSWD_summary.behav.MRIRT(idx_YA,4),2)-nanmean(STSWD_summary.behav.MRIRT(idx_YA,1),2),nanmean(STSWD_summary.brainscoreSummary.SDTaskv2(idx_YA,4),2)-nanmean(STSWD_summary.brainscoreSummary.SDTaskv2(idx_YA,1),1), 'filled'); lsline();
scatter(nanmean(STSWD_summary.behav.MRIRT(idx_OA,4),2)-nanmean(STSWD_summary.behav.MRIRT(idx_OA,1),2),nanmean(STSWD_summary.brainscoreSummary.SDTaskv2(idx_OA,4),2)-nanmean(STSWD_summary.brainscoreSummary.SDTaskv2(idx_OA,1),2), 'filled'); lsline();
[r,p] = corrcoef(nanmean(STSWD_summary.TFR.stimAlpha(idx_YA,2:4),2)-nanmean(STSWD_summary.TFR.stimAlpha(idx_YA,1),2),nanmean(STSWD_summary.brainscoreSummary.SDTaskv2(idx_YA,4),2))
[r,p] = corrcoef(nanmean(STSWD_summary.TFR.stimAlpha(idx_OA,2:4),2)-nanmean(STSWD_summary.TFR.stimAlpha(idx_OA,1),2),nanmean(STSWD_summary.brainscoreSummary.SDTaskv2(idx_OA,4),2))


figure; hold on; 
scatter(nanmean(STSWD_summary.TFR.stimAlpha(idx_YA,2:4),2)-nanmean(STSWD_summary.TFR.stimAlpha(idx_YA,1),2),nanmean(STSWD_summary.brainscoreSummary.meanTaskv2(idx_YA,2:4),2)-nanmean(STSWD_summary.brainscoreSummary.meanTaskv2(idx_YA,1),2), 'filled'); lsline();
scatter(nanmean(STSWD_summary.TFR.stimAlpha(idx_OA,2:4),2)-nanmean(STSWD_summary.TFR.stimAlpha(idx_OA,1),2),nanmean(STSWD_summary.brainscoreSummary.meanTaskv2(idx_OA,2:4),2)-nanmean(STSWD_summary.brainscoreSummary.meanTaskv2(idx_OA,1),2), 'filled'); lsline();
[r,p] = corrcoef(nanmean(STSWD_summary.TFR.stimAlpha(idx_YA,2:4),2)-nanmean(STSWD_summary.TFR.stimAlpha(idx_YA,1),2),nanmean(STSWD_summary.brainscoreSummary.meanTaskv2(idx_YA,2:4),2)-nanmean(STSWD_summary.brainscoreSummary.meanTaskv2(idx_YA,1),2))



figure; hold on; 
scatter(nanmean(STSWD_summary.TFR.stimAlpha(idx_YA,2:4),2)-nanmean(STSWD_summary.TFR.stimAlpha(idx_YA,1),2),nanmean(STSWD_summary.HDDM_ATfixed.driftMRI(idx_YA,2:4),2)-nanmean(STSWD_summary.HDDM_ATfixed.driftMRI(idx_YA,1),2), 'filled'); lsline();
scatter(nanmean(STSWD_summary.TFR.stimAlpha(idx_OA,2:4),2)-nanmean(STSWD_summary.TFR.stimAlpha(idx_OA,1),2),nanmean(STSWD_summary.HDDM_ATfixed.driftMRI(idx_OA,2:4),2)-nanmean(STSWD_summary.HDDM_ATfixed.driftMRI(idx_OA,1),2), 'filled'); lsline();
[r,p] = corrcoef(nanmean(STSWD_summary.TFR.stimAlpha(idx_YA,2:4),2)-nanmean(STSWD_summary.TFR.stimAlpha(idx_YA,1),2),nanmean(STSWD_summary.HDDM_ATfixed.driftMRI(idx_YA,2:4),2)-nanmean(STSWD_summary.HDDM_ATfixed.driftMRI(idx_YA,1),2))





figure; hold on; 
scatter(nanmean(STSWD_summary.HDDM.driftMRI(idx_YA,2:4),2)-nanmean(STSWD_summary.HDDM.driftMRI(idx_YA,1),2),nanmean(STSWD_summary.brainscoreSummary.SDTaskv2(idx_YA,2:4),2)-nanmean(STSWD_summary.brainscoreSummary.SDTaskv2(idx_YA,1),2), 'filled'); lsline();
scatter(nanmean(STSWD_summary.HDDM.driftMRI(idx_OA,2:4),2)-nanmean(STSWD_summary.HDDM.driftMRI(idx_OA,1),2),nanmean(STSWD_summary.brainscoreSummary.SDTaskv2(idx_OA,2:4),2)-nanmean(STSWD_summary.brainscoreSummary.SDTaskv2(idx_OA,1),2), 'filled'); lsline();
[r,p] = corrcoef(nanmean(STSWD_summary.HDDM.driftMRI(idx_YA,2:4),2)-nanmean(STSWD_summary.HDDM.driftMRI(idx_YA,1),2),nanmean(STSWD_summary.brainscoreSummary.SDTaskv2(idx_YA,2:4),2)-nanmean(STSWD_summary.brainscoreSummary.SDTaskv2(idx_YA,1),2))

figure; hold on; 
scatter(nanmean(STSWD_summary.HDDM.driftMRI(idx_YA,2:4),2)-nanmean(STSWD_summary.HDDM.driftMRI(idx_YA,1),2),nanmean(STSWD_summary.brainscoreSummary.meanTaskv2(idx_YA,2:4),2)-nanmean(STSWD_summary.brainscoreSummary.meanTaskv2(idx_YA,1),2), 'filled'); lsline();
scatter(nanmean(STSWD_summary.HDDM.driftMRI(idx_OA,2:4),2)-nanmean(STSWD_summary.HDDM.driftMRI(idx_OA,1),2),nanmean(STSWD_summary.brainscoreSummary.meanTaskv2(idx_OA,2:4),2)-nanmean(STSWD_summary.brainscoreSummary.meanTaskv2(idx_OA,1),2), 'filled'); lsline();
[r,p] = corrcoef(nanmean(STSWD_summary.HDDM.driftMRI(idx_YA,2:4),2)-nanmean(STSWD_summary.HDDM.driftMRI(idx_YA,1),2),nanmean(STSWD_summary.brainscoreSummary.meanTaskv2(idx_YA,2:4),2)-nanmean(STSWD_summary.brainscoreSummary.meanTaskv2(idx_YA,1),2))

figure; hold on; 
scatter(nanmean(STSWD_summary.behav.MRIRT(idx_YA,2:4),2)-nanmean(STSWD_summary.behav.MRIRT(idx_YA,1),2),nanmean(STSWD_summary.brainscoreSummary.meanTaskv2(idx_YA,2:4),2)-nanmean(STSWD_summary.brainscoreSummary.meanTaskv2(idx_YA,1),2), 'filled'); lsline();
scatter(nanmean(STSWD_summary.behav.MRIRT(idx_OA,2:4),2)-nanmean(STSWD_summary.behav.MRIRT(idx_YA,1),2),nanmean(STSWD_summary.brainscoreSummary.meanTaskv2(idx_OA,2:4),2)-nanmean(STSWD_summary.brainscoreSummary.meanTaskv2(idx_OA,1),2), 'filled'); lsline();


figure; hold on; 
scatter(nanmean(STSWD_summary.TFR.stimAlpha(idx_YA,4),2)-nanmean(STSWD_summary.TFR.stimAlpha(idx_YA,3),2),nanmean(STSWD_summary.brainscoreSummary.meanTaskv2(idx_YA,4),2)-nanmean(STSWD_summary.brainscoreSummary.meanTaskv2(idx_YA,3),2), 'filled'); lsline();
scatter(nanmean(STSWD_summary.TFR.stimAlpha(idx_OA,4),2)-nanmean(STSWD_summary.TFR.stimAlpha(idx_OA,3),2),nanmean(STSWD_summary.brainscoreSummary.meanTaskv2(idx_OA,4),2)-nanmean(STSWD_summary.brainscoreSummary.meanTaskv2(idx_OA,3),2), 'filled'); lsline();




figure; hold on; 
scatter(nanmean(STSWD_summary.HDDM_ATfixed.driftMRI(idx_YA,1),2),nanmean(STSWD_summary.brainscoreSummary.meanTaskv2(idx_YA,1),2), 'filled'); lsline();
[r,p] = corrcoef(nanmean(STSWD_summary.HDDM_ATfixed.driftMRI(idx_YA,1),2),nanmean(STSWD_summary.brainscoreSummary.meanTaskv2(idx_YA,1),2))
scatter(nanmean(STSWD_summary.HDDM_ATfixed.driftMRI(idx_OA,1),2),nanmean(STSWD_summary.brainscoreSummary.meanTaskv2(idx_OA,1),2), 'filled'); lsline();
[r,p] = corrcoef(nanmean(STSWD_summary.HDDM_ATfixed.driftMRI(idx_OA,1),2),nanmean(STSWD_summary.brainscoreSummary.meanTaskv2(idx_OA,1),2))
xlabel('Drift estimate L1'); ylabel('Mean BOLD Brainscore L1')
title('Higher Mean BOLD --> Higher drift (OA only)')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

figure; hold on; 
scatter(nanmean(STSWD_summary.HDDM.driftMRI(idx_YA,1),2),nanmean(STSWD_summary.brainscoreSummary.SDTaskv2(idx_YA,1),2), 'filled'); lsline();
scatter(nanmean(STSWD_summary.HDDM.driftMRI(idx_OA,1),2),nanmean(STSWD_summary.brainscoreSummary.SDTaskv2(idx_OA,1),2), 'filled'); lsline();


%% load brainscores from contrast 3-2 vs 1

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/D_PLS_Dim/B_data/SD_STSWD_byCond_v2/meancentContrast-4123PLS_STSWD_SD_N97_3mm_1000P1000B_byAge_BfMRIresult.mat')

groupsizes=result.num_subj_lst;
conditions=lv_evt_list;
%conditions(conditions==3) = []; % No idea why there is a condition 4 here!
conditions(conditions==5) = []; % No idea why there is a condition 5 here!

conds = {'dim1'; 'dim2'; 'dim3'; 'dim4'};

condData = []; uData = [];
for indGroup = 1:2
    if indGroup == 1
        relevantEntries = 1:groupsizes(1)*numel(conds);
    elseif indGroup == 2
        relevantEntries = groupsizes(1)*numel(conds)+1:...
             groupsizes(1)*numel(conds)+groupsizes(2)*numel(conds);
    end
    for indCond = 1:numel(conds)
        targetEntries = relevantEntries(conditions(relevantEntries)==indCond);        
        condData{indGroup}(indCond,:) = result.vsc(targetEntries,1);
        uData{indGroup}(indCond,:) = result.usc(targetEntries,1);
    end
end

contrast_3v2v1 = uData;

figure; imagesc(uData{1})
figure; imagesc(uData{2})

figure; bar(nanmean(uData{2},2))