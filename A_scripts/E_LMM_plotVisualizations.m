
pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/C_figures/E_LMMvisualizations/';

addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/T_tools/distinguishable_colors');

pn.summaryData = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/B_data/';
load([pn.summaryData, 'STSWDsummary_YA_forR.mat'],'data')

dataS = cell2table(data(2:end,:));
dataS.Properties.VariableNames = data(1,:);

variables{1,1} = 'drift_eeg'; variables{1,2} = 'eeg_lv1';
variables{2,1} = 'ndt_eeg'; variables{2,2} = 'eeg_lv1';
variables{3,1} = 'drift_eeg'; variables{3,2} = 'eeg_prestim_lv1';
variables{4,1} = 'ndt_eeg'; variables{4,2} = 'eeg_prestim_lv1';
variables{5,1} = 'eeg_lv1'; variables{5,2} = 'eeg_prestim_lv1';
variables{6,1} = 'drift_eeg'; variables{6,2} = 'eeg_lv1';

variables{7,1} = 'thetapow'; variables{7,2} = 'thetadur';
variables{8,1} = 'alphapow'; variables{8,2} = 'alphadur';
variables{9,1} = 'alphadur'; variables{9,2} = 'eeg_lv1';
variables{10,1} = 'alphapow'; variables{10,2} = 'eeg_lv1';
variables{11,1} = 'thetapow'; variables{11,2} = 'eeg_lv1';
variables{12,1} = 'thetadur'; variables{12,2} = 'eeg_lv1';

variables{13,1} = 'entropy'; variables{13,2} = 'eeg_lv1';
variables{14,1} = 'drift_eeg'; variables{14,2} = 'entropy';
variables{15,1} = 'ndt_eeg'; variables{15,2} = 'entropy';
variables{16,1} = 'aperiodic'; variables{16,2} = 'eeg_lv1';
variables{17,1} = 'aperiodic'; variables{17,2} = 'entropy';
variables{18,1} = 'aperiodic'; variables{18,2} = 'drift_eeg';
variables{19,1} = 'aperiodic'; variables{19,2} = 'ndt_eeg';
variables{20,1} = 'pupil'; variables{20,2} = 'eeg_lv1';
variables{21,1} = 'pupil'; variables{21,2} = 'entropy';
variables{22,1} = 'pupil'; variables{22,2} = 'aperiodic';

variables{23,1} = 'drift_eeg'; variables{23,2} = 'pupil';
variables{24,1} = 'ndt_eeg'; variables{24,2} = 'pupil';

% variables{25,1} = 'mri_lv1'; variables{25,2} = 'drift_mri';
% variables{26,1} = 'aperiodic'; variables{26,2} = 'entropy';
% variables{27,1} = 'aperiodic'; variables{27,2} = 'entropy';
% variables{28,1} = 'aperiodic'; variables{28,2} = 'entropy';
% variables{29,1} = 'aperiodic'; variables{29,2} = 'entropy';
% variables{30,1} = 'aperiodic'; variables{30,2} = 'entropy';

%% load summary structure

    % N = 47;
    IDs_YA = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
        '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
        '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
        '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
        '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

    % N = 42;
    IDS_YA_EEG_MR = {'1117';'1118';'1120';'1124'; '1126';'1131';'1132';'1135';'1136';...
        '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182'; '1215';...
        '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
        '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281'};


    pn.summaryData = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/B_data/';
    load([pn.summaryData, 'STSWD_summary.mat'], 'STSWD_summary')

    % select subjects (EEG only)
    idxEEG_summary = ismember(STSWD_summary.IDs, IDs_YA);
    [STSWD_summary.IDs(idxEEG_summary), IDs_YA]
    
    % select subjects (multimodal only)
    idxMulti_summary = ismember(STSWD_summary.IDs, IDS_YA_EEG_MR);
    [STSWD_summary.IDs(idxMulti_summary), IDS_YA_EEG_MR]

    cmap = distinguishable_colors(numel(unique(dataS.Subject)));
    
%% plot for chosen comparison

figure; hold on;
set(gcf,'renderer','Painters')

%for indComparison = 1:size(variables,1)

    indComparison = 20;
    cond = dataS.Condition;
    sub = dataS.Subject;
    x1 = dataS.(variables{indComparison,1});
    x2 = dataS.(variables{indComparison,2});

    tbl = table(categorical(cond),double(x1),double(x2),categorical(sub),...
        'VariableNames',{'Condition','x1','x2', 'Subject'});
    lme1 = fitlme(tbl,'x1~Subject + Condition'); R1 = residuals(lme1);
    lme2 = fitlme(tbl,'x2~Subject + Condition'); R2 = residuals(lme2);
    cmap = distinguishable_colors(size(R1,1));

    %subplot(5,5,indComparison) 
    scatter(R1, R2, 'MarkerEdgeColor', [1 1 1])
    l1 = lsline(); set(l1, 'Color',[0 0 0], 'LineWidth', 3);
    [r] = corrcoef(R1, R2);
    % choose different plotting by considering subject definition
    for indLoad = 1:4
        R1_bySub(:,indLoad) = double(R1(double(dataS.Condition)==indLoad));
        R2_bySub(:,indLoad) = double(R2(double(dataS.Condition)==indLoad));
    end
    for indID = 1:size(R1_bySub,1)
        hold on;
        scatter(R1_bySub(indID,:), R2_bySub(indID,:), [], cmap(indID,:), 'filled');
    end
    xlabel(['Residuals in ', variables{indComparison,1}]); ylabel(['Residuals in ', variables{indComparison,2}]); 
    title(['r = ', num2str(round(r(1,2),2))]);
%end


%% pupil diameter & EEG LV1

h = figure('units','normalized','position',[.1 .1 .175*2 .25]);
set(gcf,'renderer','Painters')

    ax{1} = subplot(1,2,1); cla; hold on;
    cond = dataS.Condition;
    sub = dataS.Subject;
    x1 = dataS.pupil;
    x2 = dataS.eeg_lv1;

    tbl = table(categorical(cond),double(x1),double(x2),categorical(sub),...
        'VariableNames',{'Condition','x1','x2', 'Subject'});
    lme1 = fitlme(tbl,'x1~Subject + Condition'); R1 = residuals(lme1);
    lme2 = fitlme(tbl,'x2~Subject + Condition'); R2 = residuals(lme2);

    scatter(R1, R2, 'MarkerEdgeColor', [.9 .9 .9])
    l1 = lsline(); set(l1, 'Color',[0 0 0], 'LineWidth', 3);
    [r] = corrcoef(R1, R2);
    % choose different plotting by considering subject definition
    for indLoad = 1:4
        R1_bySub(:,indLoad) = double(R1(double(dataS.Condition)==indLoad));
        R2_bySub(:,indLoad) = double(R2(double(dataS.Condition)==indLoad));
    end
    for indID = 1:size(R1_bySub,1)
        hold on;
        scatter(R1_bySub(indID,:), R2_bySub(indID,:), [], cmap(indID,:), 'filled');
    end
    xlabel([{'Individual pupil modulation'; '(Linear model residuals, mean-centered)'}]); ylabel([{'Individual Spectral Power modulation'; '(Linear model residuals, mean-centered)'}]); 
    title({['r = ', num2str(round(r(1,2),2))]});
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.9 .9 .9]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',14)
    
    % linear approximation
    
    ax{1} = subplot(1,2,2); cla; hold on;
    x = STSWD_summary.pupil2.stimdiff_slope(idxEEG_summary);
    y = STSWD_summary.EEG_LV1.slope_win(idxEEG_summary);
    scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
    [r, p] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    xlabel({'Individual pupil modulation';'(linear modulation)'}); ylabel({'Individual Spectral Power modulation';'(linear modulation)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',14)

    figureName = 'pupil_eeglv1';
    h.InvertHardcopy = 'off';

    saveas(h, [pn.plotFolder, figureName], 'fig');
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');
    
%% drift rate & EEG LV1

h = figure('units','normalized','position',[.1 .1 .175 .15]);
set(gcf,'renderer','Painters')

    ax{1} = subplot(1,2,1); cla; hold on;
    cond = dataS.Condition;
    sub = dataS.Subject;
    x1 = dataS.drift_eeg;
    x2 = dataS.eeg_lv1;

    tbl = table(categorical(cond),double(x1),double(x2),categorical(sub),...
        'VariableNames',{'Condition','x1','x2', 'Subject'});
    lme1 = fitlme(tbl,'x1~Subject + Condition'); R1 = residuals(lme1);
    lme2 = fitlme(tbl,'x2~Subject + Condition'); R2 = residuals(lme2);

    scatter(R1, R2, 'MarkerEdgeColor', [.9 .9 .9])
    xlim([min(R1)-(.3.*(max(R1)-min(R1))), max(R1)+(.3.*(max(R1)-min(R1)))])
    ylim([min(R2)-(.3.*(max(R2)-min(R2))), max(R2)+(.3.*(max(R2)-min(R2)))])
    l1 = lsline(); set(l1, 'Color',[0 0 0], 'LineWidth', 3);
    [r] = corrcoef(R1, R2);
    % choose different plotting by considering subject definition
    for indLoad = 1:4
        R1_bySub(:,indLoad) = double(R1(double(dataS.Condition)==indLoad));
        R2_bySub(:,indLoad) = double(R2(double(dataS.Condition)==indLoad));
    end
    for indID = 1:size(R1_bySub,1)
        hold on;
        scatter(R1_bySub(indID,:), R2_bySub(indID,:), [], cmap(indID,:), 'filled');
    end
    xlabel([{'Drift rate modulation'; '(mean-centered residuals)'}]); ylabel([{'Spectral Power modulation'; '(mean-centered residuals)'}]); 
    title({['r = ', num2str(round(r(1,2),2))]});
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.9 .9 .9]; end
    %xlim([-2 2]); ylim([-20 20])
    set(findall(gcf,'-property','FontSize'),'FontSize',14)
    
    % linear approximation
    
    ax{1} = subplot(1,2,2); cla; hold on;
    x = STSWD_summary.HDDM_vt.driftEEG_linear(idxEEG_summary);
    y = STSWD_summary.EEG_LV1.slope_win(idxEEG_summary);
    scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
    [r, p] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    xlabel({'Drift';'(linear modulation)'});  ylabel({'Spectral Power';'(linear modulation)'}); 
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    
    set(findall(gcf,'-property','FontSize'),'FontSize',14)

    figureName = 'eeglv1_drift';
    h.InvertHardcopy = 'off';

    saveas(h, [pn.plotFolder, figureName], 'fig');
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');

%% linear change in eeg lv1 vs. baseline NDT

h = figure('units','normalized','position',[.1 .1 .175 .15]);
set(gcf,'renderer','Painters')

        
    ax{1} = subplot(1,2,2); cla; hold on;
    x = STSWD_summary.HDDM_vt.driftEEG(idxEEG_summary,1);
    y = STSWD_summary.EEG_LV1.slope_win(idxEEG_summary);
    scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
    [r, p] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    xlabel({'Drift';'(Single Target)'});  ylabel({'Spectral Power';'(linear modulation)'}); 
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    
    set(findall(gcf,'-property','FontSize'),'FontSize',14)

    figureName = 'eeglv1_drift_baseline';
    h.InvertHardcopy = 'off';

    saveas(h, [pn.plotFolder, figureName], 'fig');
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');


    
%% ndt & EEG LV1

h = figure('units','normalized','position',[.1 .1 .175*2 .25]);
    ax{1} = subplot(1,2,1); cla; hold on;
    cond = dataS.Condition;
    sub = dataS.Subject;
    x1 = dataS.ndt_eeg;
    x2 = dataS.eeg_lv1;

    tbl = table(categorical(cond),double(x1),double(x2),categorical(sub),...
        'VariableNames',{'Condition','x1','x2', 'Subject'});
    lme1 = fitlme(tbl,'x1~Subject + Condition'); R1 = residuals(lme1);
    lme2 = fitlme(tbl,'x2~Subject + Condition'); R2 = residuals(lme2);

    scatter(R1, R2, 'MarkerEdgeColor', [.9 .9 .9])
    l1 = lsline(); set(l1, 'Color',[0 0 0], 'LineWidth', 3);
    [r] = corrcoef(R1, R2);
    % choose different plotting by considering subject definition
    for indLoad = 1:4
        R1_bySub(:,indLoad) = double(R1(double(dataS.Condition)==indLoad));
        R2_bySub(:,indLoad) = double(R2(double(dataS.Condition)==indLoad));
    end
    for indID = 1:size(R1_bySub,1)
        hold on;
        scatter(R1_bySub(indID,:), R2_bySub(indID,:), [], cmap(indID,:), 'filled');
    end
    xlabel([{'Individual NDT modulation'; '(Linear model residuals, mean-centered)'}]); ylabel([{'Individual Spectral Power modulation'; '(Linear model residuals, mean-centered)'}]); 
    title({['r = ', num2str(round(r(1,2),2))]});
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.9 .9 .9]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',14)
    
    % linear approximation
    
    ax{1} = subplot(1,2,2); cla; hold on;
    x = STSWD_summary.HDDM_vt.nondecisionEEG_linear(idxMulti_summary);
    y = STSWD_summary.EEG_LV1.slope_win(idxMulti_summary);
    scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
    [r, p] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    xlabel({'Individual NDT modulation';'(linear modulation)'}); ylabel({'Individual Spectral Power modulation';'(linear modulation)'}); 
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',14)

    figureName = 'eeglv1_ndt';
    h.InvertHardcopy = 'off';

    saveas(h, [pn.plotFolder, figureName], 'fig');
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');

    
    %% correlation between baselines: CPP threshold & HDDM threshold
    
    h = figure('units','normalized','position',[.1 .1 .175*1 .25]);
    ax{1} = subplot(1,1,1); cla; hold on;
    x = STSWD_summary.HDDM.nondecisionEEG(idxMulti_summary,1);
    y = STSWD_summary.NDTpotential.data(idxMulti_summary,1);
    scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
    [r, p] = corrcoef(x, y);
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    xlabel({' NDT ';'(l1)'}); ylabel({'NDTpotential';'(l1)'}); 
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',14)
    
    h = figure('units','normalized','position',[.1 .1 .175*1 .25]);
    ax{1} = subplot(1,1,1); cla; hold on;
    x = nanmean(STSWD_summary.HDDM.thresholdEEG(idxMulti_summary,1:4),2);
    y = nanmean(STSWD_summary.ThetaResp.data(idxMulti_summary,1:4),2);
    scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
    [r, p] = corrcoef(x, y);
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    xlabel({'HDDM threshold estimate';'(avg. across loads)'}); ylabel({'Reponse-related theta power';'(avg. across loads)'}); 
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',14)
    
    h = figure('units','normalized','position',[.1 .1 .175*1 .25]);
    ax{1} = subplot(1,1,1); cla; hold on;
    x = nanmean(STSWD_summary.HDDM_vt.thresholdEEG(idxMulti_summary,1),2);
    y = nanmean(STSWD_summary.ThetaResp.data(idxMulti_summary,1:4),2);
    scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
    [r, p] = corrcoef(x, y);
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    xlabel({'HDDM threshold estimate';'(avg. across loads)'}); ylabel({'Reponse-related theta power';'(avg. across loads)'}); 
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',14)
    
    h = figure('units','normalized','position',[.1 .1 .175*1 .25]);
    ax{1} = subplot(1,1,1); cla; hold on;
    x = nanmean(STSWD_summary.HDDM_vt.driftEEG(idxMulti_summary,1),2);
    y = nanmean(STSWD_summary.HDDM_vt.thresholdEEG(idxMulti_summary,1),2);
    scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
    [r, p] = corrcoef(x, y);
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    xlabel({'HDDM threshold estimate';'(avg. across loads)'}); ylabel({'Reponse-related theta power';'(avg. across loads)'}); 
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',14)
    
    h = figure('units','normalized','position',[.1 .1 .175*1 .25]);
    ax{1} = subplot(1,1,1); cla; hold on;
    x = nanmean(STSWD_summary.OneFslope.data(idxMulti_summary,1:4),2);
    y = nanmean(STSWD_summary.SSVEPmag.data_norm(idxMulti_summary,1:4),2);
    scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
    [r, p] = corrcoef(x, y);
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    xlabel({'OneFslope';'(avg. across loads)'}); ylabel({'SSVEPmag';'(avg. across loads)'}); 
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',14)
    
    h = figure('units','normalized','position',[.1 .1 .175*1 .25]);
    ax{1} = subplot(1,1,1); cla; hold on;
    x =  nanmean(STSWD_summary.ThetaResp.data(idxMulti_summary,1),2);
    y = nanmean(STSWD_summary.CPPthreshold.data(idxMulti_summary,1),2);
    scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
    [r, p] = corrcoef(x, y);
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    xlabel({'HDDM threshold estimate';'(avg. across loads)'}); ylabel({'Reponse-related theta power';'(avg. across loads)'}); 
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',14)
    
    % larger frontal theta power around response --> lower threshold
    % lower threshold --> more liberal/impulsive, less decision conflict
    
    h = figure('units','normalized','position',[.1 .1 .175*1 .25]);
    ax{1} = subplot(1,1,1); cla; hold on;
    x = nanmean(STSWD_summary.HDDM.thresholdEEG(idxMulti_summary,1:4),2);
    y = nanmean(STSWD_summary.CPPthreshold.data(idxMulti_summary,1:4),2);
    scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
    [r, p] = corrcoef(x, y);
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    xlabel({'HDDM threshold estimate';'(avg. across loads)'}); ylabel({'Reponse-related theta power';'(avg. across loads)'}); 
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',14)
    
    %%
    
    h = figure('units','normalized','position',[.1 .1 .175*1 .25]);
    ax{1} = subplot(1,1,1); cla; hold on;
    x = nanmean(STSWD_summary.HDDM_vt.thresholdEEG(idxMulti_summary,1:4),2);
    y = nanmean(STSWD_summary.CPPthreshold.data(idxMulti_summary,1:4),2);
    scatter(x, y, 70, 'filled', 'MarkerFaceColor', [.8 .8 .8]); l1 = lsline(); set(l1, 'Color',[.8 .8 .8], 'LineWidth', 3);
    [r, p] = corrcoef(x, y);
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    xlabel({'HDDM threshold estimate';'(avg. across loads)'}); ylabel({'CPP threshold';'(avg. across loads)'}); 
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',14)
    
    
    h = figure('units','normalized','position',[.1 .1 .175*1 .25]);
    ax{1} = subplot(1,1,1); cla; hold on;
    x = STSWD_summary.HDDM_vt.driftEEG(idxMulti_summary,1);
    y = STSWD_summary.CPPSlope.data(idxMulti_summary,1);
    scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
    [r, p, PL, PU] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
    title(['r = ', num2str(round(r(2),2)), ' [', num2str(round(PL(2),2)),',', num2str(round(PU(2),2))  '];', ' p = ', num2str(round(p(2),3))])
    xlabel({'Individual NDT modulation';'(linear modulation)'}); ylabel({'Individual Spectral Power modulation';'(linear modulation)'}); 
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',14)
 
    load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/B_data/I2_IndividualERP.mat', 'IndividualERP');
    
%     h = figure('units','normalized','position',[.1 .1 .175*1 .25]);
%     ax{1} = subplot(1,1,1); cla; hold on;
%     x = IndividualERP.data_linear(:,1);
%     y = STSWD_summary.EEG_prestim.slope(idxEEG_summary,1);
%     scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
%     [r, p, PL, PU] = corrcoef(x, y);
% % 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
%     title(['r = ', num2str(round(r(2),2)), ' [', num2str(round(PL(2),2)),',', num2str(round(PU(2),2))  '];', ' p = ', num2str(round(p(2),3))])
%     xlabel({'Individual NDT modulation';'(linear modulation)'}); ylabel({'Individual Spectral Power modulation';'(linear modulation)'}); 
%     ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
%     set(h,'Color','w')
%     for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
%     set(findall(gcf,'-property','FontSize'),'FontSize',14)
