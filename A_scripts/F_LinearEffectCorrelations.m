% assess inter-individual relations between first level linear effects 

% N = 47;
IDs_YA = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

% N = 42;
IDS_YA_EEG_MR = {'1117';'1118';'1120';'1124'; '1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182'; '1215';...
    '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
    '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281'};

%% load summary structure

    pn.summaryData = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/B_data/';
    load([pn.summaryData, 'STSWD_summary.mat'], 'STSWD_summary')

    % select subjects (EEG only)
    idxEEG_summary = ismember(STSWD_summary.IDs, IDs_YA);
    [STSWD_summary.IDs(idxEEG_summary), IDs_YA]
    
    % select subjects (multimodal only)
    idxMulti_summary = ismember(STSWD_summary.IDs, IDS_YA_EEG_MR);
    [STSWD_summary.IDs(idxMulti_summary), IDS_YA_EEG_MR]
    
%% perform 2nd level correlations

%% EEG LV 1 - drift

h = figure('units','normalized','position',[.1 .1 .3 .25]);
    ax{1} = subplot(1,1,1); cla; hold on;
    x = STSWD_summary.EEG_LV1.slope_win(idxMulti_summary);
    y = STSWD_summary.HDDM_vt.driftEEG_linear(idxMulti_summary);
	scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
	[r, p] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    xlabel({'EEG LV1';'(linear modulation)'}); ylabel({'Drift rate';'(linear modulation)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',14)

%% EEG LV 1 - ndt

h = figure('units','normalized','position',[.1 .1 .175 .25]);
    ax{1} = subplot(1,1,1); cla; hold on;
    x = STSWD_summary.EEG_LV1.slope_win(idxMulti_summary);
    y = STSWD_summary.HDDM_vt.nondecisionEEG_linear(idxMulti_summary);
	scatter(x, y, 70, 'filled', 'MarkerFaceColor', [.8 .8 .8]); l1 = lsline(); set(l1, 'Color',[.8 .8 .8], 'LineWidth', 3);
	[r, p] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    xlabel({'EEG LV1';'(linear modulation)'}); ylabel({'nondecision time';'(linear modulation)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',14)
    
%% EEG LV 1 - SE

h = figure('units','normalized','position',[.1 .1 .3 .25]);
    ax{1} = subplot(1,1,1); cla; hold on;
    x = STSWD_summary.EEG_LV1.slope_win(idxMulti_summary);
    y = STSWD_summary.SE.data_slope_win(idxMulti_summary);
	scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
	[r, p] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    xlabel({'EEG LV1';'(linear modulation)'}); ylabel({'sample entropy';'(linear modulation)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',14)
    
%% 1/f slopes - SE

h = figure('units','normalized','position',[.1 .1 .175 .25]);
    ax{1} = subplot(1,1,1); cla; hold on;
    x = STSWD_summary.OneFslope.linear_win(idxMulti_summary);
    y = STSWD_summary.SE.data_slope_win(idxMulti_summary);
	scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
	[r, p] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    xlabel({'1/f slopes';'(linear modulation)'}); ylabel({'sample entropy';'(linear modulation)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',14)
    
    
%% 1/f slopes - eeg lv 1

h = figure('units','normalized','position',[.1 .1 .175 .25]);
    ax{1} = subplot(1,1,1); cla; hold on;
    x = STSWD_summary.OneFslope.linear_win(idxMulti_summary);
    x = STSWD_summary.EEG_LV1.slope_win(idxMulti_summary);
	scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
	[r, p] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    xlabel({'1/f slopes';'(linear modulation)'}); ylabel({'eeg lv 1';'(linear modulation)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',14)
    
    
%% mri lv 1 - drift mri

h = figure('units','normalized','position',[.1 .1 .175 .25]);
    ax{1} = subplot(1,1,1); cla; hold on;
    x = STSWD_summary.SPM_task.LV1.slope(idxMulti_summary);
    y = STSWD_summary.HDDM_vt.driftMRI_linear(idxMulti_summary);
	scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
	[r, p] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    xlabel({'MRI task LV1';'(linear modulation)'}); ylabel({'drift rate MRI';'(linear modulation)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',14)
    
    
%% mri task lv1 - pupil

h = figure('units','normalized','position',[.1 .1 .175 .25]);
    ax{1} = subplot(1,1,1); cla; hold on;
    x = STSWD_summary.SPM_task.LV1.slope(idxMulti_summary);
    y = STSWD_summary.pupil2.stimdiff_slope(idxMulti_summary);
	scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
	[r, p] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    xlabel({'MRI task LV1';'(linear modulation)'}); ylabel({'pupil derivative';'(linear modulation)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',14)

%% mri lv 2 - ndt mri

h = figure('units','normalized','position',[.1 .1 .175 .25]);
    ax{1} = subplot(1,1,1); cla; hold on;
    x = STSWD_summary.SPM_task.LV2.slope(idxMulti_summary);
    y = STSWD_summary.HDDM_vt.nondecisionMRI_linear(idxMulti_summary);
	scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
	[r, p] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    xlabel({'MRI task LV2';'(linear modulation)'}); ylabel({'ndt MRI';'(linear modulation)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',14)
    
h = figure('units','normalized','position',[.1 .1 .175 .25]);
    ax{1} = subplot(1,1,1); cla; hold on;
    x = STSWD_summary.SPM_task.LV2.slope(idxMulti_summary);
    y = STSWD_summary.EEG_LV1.slope_win(idxMulti_summary);
	scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
	[r, p] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    xlabel({'MRI task LV2';'(linear modulation)'}); ylabel({'eeg lv1';'(linear modulation)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',14)

 %% eeg lv1 - pupil

h = figure('units','normalized','position',[.1 .1 .175 .25]);
    ax{1} = subplot(1,1,1); cla; hold on;
    x = STSWD_summary.EEG_LV1.slope_win(idxMulti_summary);
    y = STSWD_summary.pupil2.stimdiff_slope(idxMulti_summary);
	scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
	[r, p] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    xlabel({'eeg LV1';'(linear modulation)'}); ylabel({'pupil deirvative';'(linear modulation)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',14)
    
    
%% drift eeg - pupil
% stronger increase in phasic pupil dilation - stronger drift decreases

h = figure('units','normalized','position',[.1 .1 .175 .25]);
    ax{1} = subplot(1,1,1); cla; hold on;
    x = STSWD_summary.HDDM_vt.driftEEG_linear(idxMulti_summary);
    y = STSWD_summary.pupil2.stimdiff_slope(idxMulti_summary);
	scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
	[r, p] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    xlabel({'drift eeg';'(linear modulation)'}); ylabel({'pupil deirvative';'(linear modulation)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',14)
    
    
%% ndt eeg - pupil
% stronger increase in phasic pupil dilation - constrained ndt increases

h = figure('units','normalized','position',[.1 .1 .175 .25]);
    ax{1} = subplot(1,1,1); cla; hold on;
    x = STSWD_summary.HDDM_vt.nondecisionEEG_linear(idxMulti_summary);
    y = STSWD_summary.pupil2.stimdiff_slope(idxMulti_summary);
	scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
	[r, p] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    xlabel({'ndt eeg';'(linear modulation)'}); ylabel({'pupil deirvative';'(linear modulation)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',14)
    
%% 1/f and entropy

h = figure('units','normalized','position',[.1 .1 .175 .25]);
    ax{1} = subplot(1,1,1); cla; hold on;
    x = squeeze(nanmean(STSWD_summary.OneFslope.data(idxEEG_summary,:),2));
    y = squeeze(nanmean(STSWD_summary.SE.data(idxEEG_summary,:),2));
	scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
	[r, p] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    xlabel({'1/f'}); ylabel({'entropy'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',14)
