
pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/C_figures/E_LMMvisualizations/';

addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/T_tools/distinguishable_colors');

pn.summaryData = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/B_data/';
load([pn.summaryData, 'STSWDsummary_YA_forR.mat'],'data')

dataS = cell2table(data(2:end,:));
dataS.Properties.VariableNames = data(1,:);

%% load summary structure

    % N = 47;
    IDs_YA = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
        '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
        '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
        '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
        '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

    % N = 42;
    IDS_YA_EEG_MR = {'1117';'1118';'1120';'1124'; '1126';'1131';'1132';'1135';'1136';...
        '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182'; '1215';...
        '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
        '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281'};


    pn.summaryData = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/B_data/';
    load([pn.summaryData, 'STSWD_summary.mat'], 'STSWD_summary')

    % select subjects (EEG only)
    idxEEG_summary = ismember(STSWD_summary.IDs, IDs_YA);
    [STSWD_summary.IDs(idxEEG_summary), IDs_YA]
    
    % select subjects (multimodal only)
    idxMulti_summary = ismember(STSWD_summary.IDs, IDS_YA_EEG_MR);
    [STSWD_summary.IDs(idxMulti_summary), IDS_YA_EEG_MR]
    idxMRI_dataS = ismember(dataS_MRI.Subject, IDS_YA_EEG_MR);

    cmap = distinguishable_colors(numel(unique(dataS.Subject)));
    
%% drift & ndt (EEG)

h = figure('units','normalized','position',[.1 .1 .175*2 .25]);
    ax{1} = subplot(1,2,1); cla; hold on;
    cond = dataS.Condition;
    sub = dataS.Subject;
    x1 = dataS.drift_eeg;
    x2 = dataS.ndt_eeg;

    tbl = table(categorical(cond),double(x1),double(x2),categorical(sub),...
        'VariableNames',{'Condition','x1','x2', 'Subject'});
    lme1 = fitlme(tbl,'x1~Subject + Condition'); R1 = residuals(lme1);
    lme2 = fitlme(tbl,'x2~Subject + Condition'); R2 = residuals(lme2);

    scatter(R1, R2, 'MarkerEdgeColor', [.9 .9 .9])
    l1 = lsline(); set(l1, 'Color',[0 0 0], 'LineWidth', 3);
    [r] = corrcoef(R1, R2);
    % choose different plotting by considering subject definition
    for indLoad = 1:4
        R1_bySub(:,indLoad) = double(R1(double(dataS.Condition)==indLoad));
        R2_bySub(:,indLoad) = double(R2(double(dataS.Condition)==indLoad));
    end
    for indID = 1:size(R1_bySub,1)
        hold on;
        scatter(R1_bySub(indID,:), R2_bySub(indID,:), [], cmap(indID,:), 'filled');
    end
    xlabel([{'Individual pupil modulation'; '(Linear model residuals, mean-centered)'}]); ylabel([{'Individual Spectral Power modulation'; '(Linear model residuals, mean-centered)'}]); 
    title({['r = ', num2str(round(r(1,2),2))]});
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.9 .9 .9]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',14)
    
    % linear approximation
    
    ax{1} = subplot(1,2,2); cla; hold on;
    x = STSWD_summary.pupil2.stimdiff_slope(idxEEG_summary);
    y = STSWD_summary.EEG_LV1.slope_win(idxEEG_summary);
    scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
    [r, p] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    xlabel({'Individual pupil modulation';'(linear modulation)'}); ylabel({'Individual Spectral Power modulation';'(linear modulation)'})
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',14)

%     figureName = 'pupil_eeglv1';
%     h.InvertHardcopy = 'off';
% 
%     saveas(h, [pn.plotFolder, figureName], 'fig');
%     saveas(h, [pn.plotFolder, figureName], 'epsc');
%     saveas(h, [pn.plotFolder, figureName], 'png');


%% linear change in drift vs. nondecison: EEG

h = figure('units','normalized','position',[.1 .1 .3 .15]);
    
    ax{1} = subplot(1,3,1); cla; hold on;
    cond = dataS.Condition;
    sub = dataS.Subject;
    x1 = dataS.drift_eeg;
    x2 = dataS.ndt_eeg;

    tbl = table(categorical(cond),double(x1),double(x2),categorical(sub),...
        'VariableNames',{'Condition','x1','x2', 'Subject'});
    lme1 = fitlme(tbl,'x1~Subject + Condition'); R1 = residuals(lme1);
    lme2 = fitlme(tbl,'x2~Subject + Condition'); R2 = residuals(lme2);

    x = R1; y = R2;
    scatter(x,y, 'MarkerEdgeColor', [.9 .9 .9])
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    xlim([min(x)-(.1.*(max(x)-min(x))), max(x)+(.1.*(max(x)-min(x)))])
    l1 = lsline(); set(l1, 'Color',[0 0 0], 'LineWidth', 3);
    [r] = corrcoef(R1, R2);
    % choose different plotting by considering subject definition
    R1_bySub = []; R2_bySub = [];
    for indLoad = 1:4
        R1_bySub(:,indLoad) = double(R1(double(dataS.Condition)==indLoad));
        R2_bySub(:,indLoad) = double(R2(double(dataS.Condition)==indLoad));
    end
    for indID = 1:size(R1_bySub,1)
        hold on;
        scatter(R1_bySub(indID,:), R2_bySub(indID,:), [], cmap(indID,:), 'filled');
    end
    xlabel([{'Drift modulation'; '(mean-centered)'}]); ylabel([{'Nondecision modulation'; '(mean-centered)'}]); 
    title({['r = ', num2str(round(r(1,2),2))]});
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.9 .9 .9]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',14)
    
    % plot intercept-change correlation

    ax{1} = subplot(1,3,2); cla; hold on;
    x = STSWD_summary.HDDM_vt.driftEEG(idxEEG_summary,1);
    y = STSWD_summary.HDDM_vt.nondecisionEEG_linear(idxEEG_summary,1);
    scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
    [r, p] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    xlabel({'Drift';'(Single Target)'});  ylabel({'Nondecision time';'(linear modulation)'}); 
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    xlim([min(x)-(.1.*(max(x)-min(x))), max(x)+(.1.*(max(x)-min(x)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    
    ax{1} = subplot(1,3,3); cla; hold on;
    x = STSWD_summary.HDDM_vt.driftEEG_linear(idxEEG_summary,1);
    y = STSWD_summary.HDDM_vt.nondecisionEEG(idxEEG_summary,1);
    scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
    [r, p] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    xlabel({'Drift';'(linear modulation)'});  ylabel({'Nondecision time';'(Single Target)'}); 
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    xlim([min(x)-(.1.*(max(x)-min(x))), max(x)+(.1.*(max(x)-min(x)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    
    set(findall(gcf,'-property','FontSize'),'FontSize',14)

    figureName = 'ndt_drift_eeg';
    h.InvertHardcopy = 'off';

    saveas(h, [pn.plotFolder, figureName], 'fig');
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');

%% linear change in drift vs. nondecison: MRI

h = figure('units','normalized','position',[.1 .1 .3 .15]);
    
    ax{1} = subplot(1,3,1); cla; hold on;
    cond = dataS.Condition(idxMRI_dataS);
    sub = dataS.Subject(idxMRI_dataS);
    x1 = dataS.drift_mri(idxMRI_dataS,:);
    x2 = dataS.ndt_mri(idxMRI_dataS,:);

    tbl = table(categorical(cond),double(x1),double(x2),categorical(sub),...
        'VariableNames',{'Condition','x1','x2', 'Subject'});
    lme1 = fitlme(tbl,'x1~Subject + Condition'); R1 = residuals(lme1);
    lme2 = fitlme(tbl,'x2~Subject + Condition'); R2 = residuals(lme2);

    x = R1; y = R2;
    scatter(x,y, 'MarkerEdgeColor', [.9 .9 .9])
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    xlim([min(x)-(.1.*(max(x)-min(x))), max(x)+(.1.*(max(x)-min(x)))])
    l1 = lsline(); set(l1, 'Color',[0 0 0], 'LineWidth', 3);
    [r] = corrcoef(R1, R2);
    % choose different plotting by considering subject definition
    R1_bySub = []; R2_bySub = [];
    for indLoad = 1:4
        R1_bySub(:,indLoad) = double(R1(double(cond)==indLoad));
        R2_bySub(:,indLoad) = double(R2(double(cond)==indLoad));
    end
    for indID = 1:size(R1_bySub,1)
        hold on;
        scatter(R1_bySub(indID,:), R2_bySub(indID,:), [], cmap(indID,:), 'filled');
    end
    xlabel([{'Drift modulation'; '(mean-centered)'}]); ylabel([{'Nondecision modulation'; '(mean-centered)'}]); 
    title({['r = ', num2str(round(r(1,2),2))]});
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.9 .9 .9]; end
    set(findall(gcf,'-property','FontSize'),'FontSize',14)
    
    % plot intercept-change correlation

    ax{1} = subplot(1,3,2); cla; hold on;
    x = STSWD_summary.HDDM_vt.driftMRI(idxMulti_summary,1);
    y = STSWD_summary.HDDM_vt.nondecisionMRI_linear(idxMulti_summary,1);
    scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
    [r, p] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    xlabel({'Drift';'(Single Target)'});  ylabel({'Nondecision time';'(linear modulation)'}); 
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    xlim([min(x)-(.1.*(max(x)-min(x))), max(x)+(.1.*(max(x)-min(x)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    
    ax{1} = subplot(1,3,3); cla; hold on;
    x = STSWD_summary.HDDM_vt.driftMRI_linear(idxMulti_summary,1);
    y = STSWD_summary.HDDM_vt.nondecisionMRI(idxMulti_summary,1);
    scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
    [r, p] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    xlabel({'Drift';'(linear modulation)'});  ylabel({'Nondecision time';'(Single Target)'}); 
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    xlim([min(x)-(.1.*(max(x)-min(x))), max(x)+(.1.*(max(x)-min(x)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    
    set(findall(gcf,'-property','FontSize'),'FontSize',14)

    figureName = 'ndt_drift_mri';
    h.InvertHardcopy = 'off';

    saveas(h, [pn.plotFolder, figureName], 'fig');
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');

%% linear change in drift & drift baseline

    h = figure('units','normalized','position',[.1 .1 .45 .15]);

    ax{1} = subplot(1,4,1); cla; hold on;
    x = STSWD_summary.HDDM_vt.driftEEG(idxEEG_summary,1);
    y = STSWD_summary.HDDM_vt.thresholdEEG(idxEEG_summary,1);
    scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
    [r, p] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    xlabel({'Drift';'(Single Target)'});  ylabel({'Threshold';'(Single Target)'}); 
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    xlim([min(x)-(.1.*(max(x)-min(x))), max(x)+(.1.*(max(x)-min(x)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    
    ax{1} = subplot(1,4,2); cla; hold on;
    x = STSWD_summary.HDDM_vt.driftMRI(idxMulti_summary,1);
    y = STSWD_summary.HDDM_vt.thresholdMRI(idxMulti_summary,1);
    scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
    [r, p] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    xlabel({'Drift';'(Single Target)'});  ylabel({'Threshold';'(Single Target)'}); 
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    xlim([min(x)-(.1.*(max(x)-min(x))), max(x)+(.1.*(max(x)-min(x)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    
    ax{1} = subplot(1,4,3); cla; hold on;
    x = STSWD_summary.HDDM_vt.driftEEG(idxEEG_summary,1);
    y = STSWD_summary.HDDM_vt.driftEEG_linear(idxEEG_summary,1);
    scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
    [r, p] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    xlabel({'Drift';'(Single Target)'});  ylabel({'Drift';'(linear modulation)'}); 
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    xlim([min(x)-(.1.*(max(x)-min(x))), max(x)+(.1.*(max(x)-min(x)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end
    
    ax{1} = subplot(1,4,4); cla; hold on;
    x = STSWD_summary.HDDM_vt.driftMRI(idxMulti_summary,1);
    y = STSWD_summary.HDDM_vt.driftMRI_linear(idxMulti_summary,1);
    scatter(x, y, 70, 'filled', 'MarkerFaceColor', [1 .6 .6]); l1 = lsline(); set(l1, 'Color',[1 .6 .6], 'LineWidth', 3);
    [r, p] = corrcoef(x, y);
% 	legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))],'location', 'SouthEast'); legend('boxoff')   
    title(['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),3))])
    xlabel({'Drift';'(Single Target)'});  ylabel({'Drift';'(linear modulation)'}); 
    ylim([min(y)-(.1.*(max(y)-min(y))), max(y)+(.1.*(max(y)-min(y)))])
    xlim([min(x)-(.1.*(max(x)-min(x))), max(x)+(.1.*(max(x)-min(x)))])
    set(h,'Color','w')
    for indAx = 1:1; ax{indAx}.Color = [.2 .2 .2]; end