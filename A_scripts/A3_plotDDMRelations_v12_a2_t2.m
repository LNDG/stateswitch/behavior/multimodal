%% Plot associations between multimodal data

pn.dataOut = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/B_data/';
load([pn.dataOut, 'STSWD_summary.mat'], 'STSWD_summary')

dataPath = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/D_DDM/B_data/';
load([dataPath, 'HDDM_summary_v12_a2_t2.mat'], 'HDDM_summary_v12_a2_t2')

idxSummary = ismember(STSWD_summary.IDs, HDDM_summary_v12_a2_t2.IDs);

idx_YA = cellfun(@str2num, HDDM_summary_v12_a2_t2.IDs)<2000;
idx_OA = cellfun(@str2num, HDDM_summary_v12_a2_t2.IDs)>2000;

%% plot HDDM parameters by load & age

% threshold doesn't appear to strongly vary by age, maybe drop via model selection?

figure; 
subplot(2,2,1); bar(nanmean(HDDM_summary_v12_a2_t2.nondecisionEEG(idx_YA,:),1)); title('YA EEG'); xlabel('Load')
subplot(2,2,2); bar(nanmean(HDDM_summary_v12_a2_t2.nondecisionEEG(idx_OA,:),1)); title('OA EEG'); xlabel('Load')
subplot(2,2,3); bar(nanmean(HDDM_summary_v12_a2_t2.nondecisionMRI(idx_YA,:),1)); title('YA MRI'); xlabel('Load')
subplot(2,2,4); bar(nanmean(HDDM_summary_v12_a2_t2.nondecisionMRI(idx_OA,:),1)); title('OA MRI'); xlabel('Load')
suptitle('HDDM NDT')

figure; 
subplot(2,2,1); bar(nanmean(HDDM_summary_v12_a2_t2.thresholdEEG(idx_YA,:),1)); title('YA EEG'); xlabel('Load')
subplot(2,2,2); bar(nanmean(HDDM_summary_v12_a2_t2.thresholdEEG(idx_OA,:),1)); title('OA EEG'); xlabel('Load')
subplot(2,2,3); bar(nanmean(HDDM_summary_v12_a2_t2.thresholdMRI(idx_YA,:),1)); title('YA MRI'); xlabel('Load')
subplot(2,2,4); bar(nanmean(HDDM_summary_v12_a2_t2.thresholdMRI(idx_OA,:),1)); title('OA MRI'); xlabel('Load')
suptitle('HDDM Threshold')

figure; 
subplot(2,2,1); bar(nanmean(HDDM_summary_v12_a2_t2.driftEEG(idx_YA,:),1)); title('YA EEG'); xlabel('Load')
subplot(2,2,2); bar(nanmean(HDDM_summary_v12_a2_t2.driftEEG(idx_OA,:),1)); title('OA EEG'); xlabel('Load')
subplot(2,2,3); bar(nanmean(HDDM_summary_v12_a2_t2.driftMRI(idx_YA,:),1)); title('YA MRI'); xlabel('Load')
subplot(2,2,4); bar(nanmean(HDDM_summary_v12_a2_t2.driftMRI(idx_OA,:),1)); title('OA MRI'); xlabel('Load')
suptitle('HDDM Drift')

%% plot DDM parameters by load & age

% figure; 
% subplot(3,4,1); bar(nanmean(HDDM_summary_v12_a2_t2.nondecisionEEG(idx_YA,:),1)); ylim([0 .5]); title('NDT: YA EEG'); xlabel('Load')
% subplot(3,4,2); bar(nanmean(HDDM_summary_v12_a2_t2.nondecisionEEG(idx_OA,:),1)); ylim([0 .5]); title('NDT: OA EEG'); xlabel('Load')
% subplot(3,4,3); bar(nanmean(HDDM_summary_v12_a2_t2.nondecisionMRI(idx_YA,:),1)); ylim([0 .5]); title('NDT: YA MRI'); xlabel('Load')
% subplot(3,4,4); bar(nanmean(HDDM_summary_v12_a2_t2.nondecisionMRI(idx_OA,:),1)); ylim([0 .5]); title('NDT: OA MRI'); xlabel('Load')
% 
% subplot(3,4,4+1); bar(nanmean(HDDM_summary_v12_a2_t2.thresholdEEG(idx_YA,:),1)); ylim([0 1.5]); title('Threshold: YA EEG'); xlabel('Load')
% subplot(3,4,4+2); bar(nanmean(HDDM_summary_v12_a2_t2.thresholdEEG(idx_OA,:),1), [0 1.5]); title('Threshold: OA EEG'); xlabel('Load')
% subplot(3,4,4+3); bar(nanmean(HDDM_summary_v12_a2_t2.thresholdMRI(idx_YA,:),1), [0 1.5]); title('Threshold: YA MRI'); xlabel('Load')
% subplot(3,4,4+4); bar(nanmean(HDDM_summary_v12_a2_t2.thresholdMRI(idx_OA,:),1), [0 1.5]); title('Threshold: OA MRI'); xlabel('Load')
% 
% subplot(3,4,8+1); bar(nanmean(HDDM_summary_v12_a2_t2.driftEEG(idx_YA,:),1), [0 2]); title('Drift: YA EEG'); xlabel('Load')
% subplot(3,4,8+2); bar(nanmean(HDDM_summary_v12_a2_t2.driftEEG(idx_OA,:),1), [0 2]); title('Drift: OA EEG'); xlabel('Load')
% subplot(3,4,8+3); bar(nanmean(HDDM_summary_v12_a2_t2.driftMRI(idx_YA,:),1), [0 2]); title('Drift: YA MRI'); xlabel('Load')
% subplot(3,4,8+4); bar(nanmean(HDDM_summary_v12_a2_t2.driftMRI(idx_OA,:),1), [0 2]); title('Drift: OA MRI'); xlabel('Load')

%% plot slope reliability (change 1-2, 2-3, 3-4)

figure;
subplot(2,3,1); hold on;
tmp_yEEG = HDDM_summary_v12_a2_t2.thresholdEEG(idx_YA,1);
tmp_yMRI = HDDM_summary_v12_a2_t2.thresholdMRI(idx_YA,1);
scatter(tmp_yEEG, tmp_yMRI, 'filled')
tmp_yEEG = HDDM_summary_v12_a2_t2.thresholdEEG(idx_OA,1);
tmp_yMRI = HDDM_summary_v12_a2_t2.thresholdMRI(idx_OA,1);
scatter(tmp_yEEG, tmp_yMRI, 'filled')
title('Reliability threshold L1')
subplot(2,3,2); hold on;
tmp_yEEG = HDDM_summary_v12_a2_t2.driftEEG(idx_YA,1);
tmp_yMRI = HDDM_summary_v12_a2_t2.driftMRI(idx_YA,1);
scatter(tmp_yEEG, tmp_yMRI, 'filled')
tmp_yEEG = HDDM_summary_v12_a2_t2.driftEEG(idx_OA,1);
tmp_yMRI = HDDM_summary_v12_a2_t2.driftMRI(idx_OA,1);
scatter(tmp_yEEG, tmp_yMRI, 'filled')
title('Reliability drift L1')
subplot(2,3,3); hold on;
tmp_yEEG = HDDM_summary_v12_a2_t2.nondecisionEEG(idx_YA,1);
tmp_yMRI = HDDM_summary_v12_a2_t2.nondecisionMRI(idx_YA,1);
scatter(tmp_yEEG, tmp_yMRI, 'filled')
tmp_yEEG = HDDM_summary_v12_a2_t2.nondecisionEEG(idx_OA,1);
tmp_yMRI = HDDM_summary_v12_a2_t2.nondecisionMRI(idx_OA,1);
scatter(tmp_yEEG, tmp_yMRI, 'filled')
title('Reliability NDT L1')

subplot(2,3,4); hold on;
tmp_yEEG = HDDM_summary_v12_a2_t2.thresholdEEG(idx_YA,4)-HDDM_summary_v12_a2_t2.thresholdEEG(idx_YA,1);
tmp_yMRI = HDDM_summary_v12_a2_t2.thresholdMRI(idx_YA,4)-HDDM_summary_v12_a2_t2.thresholdMRI(idx_YA,1);
scatter(tmp_yEEG, tmp_yMRI, 'filled')
tmp_yEEG = HDDM_summary_v12_a2_t2.thresholdEEG(idx_OA,4)-HDDM_summary_v12_a2_t2.thresholdEEG(idx_OA,1);
tmp_yMRI = HDDM_summary_v12_a2_t2.thresholdMRI(idx_OA,4)-HDDM_summary_v12_a2_t2.thresholdMRI(idx_OA,1);
scatter(tmp_yEEG, tmp_yMRI, 'filled')
title('Reliability threshold L4-1')
subplot(2,3,5); hold on;
tmp_yEEG = HDDM_summary_v12_a2_t2.driftEEG(idx_YA,4)-HDDM_summary_v12_a2_t2.driftEEG(idx_YA,1);
tmp_yMRI = HDDM_summary_v12_a2_t2.driftMRI(idx_YA,4)-HDDM_summary_v12_a2_t2.driftMRI(idx_YA,1);
scatter(tmp_yEEG, tmp_yMRI, 'filled')
tmp_yEEG = HDDM_summary_v12_a2_t2.driftEEG(idx_OA,4)-HDDM_summary_v12_a2_t2.driftEEG(idx_OA,1);
tmp_yMRI = HDDM_summary_v12_a2_t2.driftMRI(idx_OA,4)-HDDM_summary_v12_a2_t2.driftMRI(idx_OA,1);
scatter(tmp_yEEG, tmp_yMRI, 'filled')
title('Reliability drift L4-1')
subplot(2,3,6); hold on;
tmp_yEEG = HDDM_summary_v12_a2_t2.nondecisionEEG(idx_YA,4)-HDDM_summary_v12_a2_t2.nondecisionEEG(idx_YA,1);
tmp_yMRI = HDDM_summary_v12_a2_t2.nondecisionMRI(idx_YA,4)-HDDM_summary_v12_a2_t2.nondecisionMRI(idx_YA,1);
scatter(tmp_yEEG, tmp_yMRI, 'filled')
tmp_yEEG = HDDM_summary_v12_a2_t2.nondecisionEEG(idx_OA,4)-HDDM_summary_v12_a2_t2.nondecisionEEG(idx_OA,1);
tmp_yMRI = HDDM_summary_v12_a2_t2.nondecisionMRI(idx_OA,4)-HDDM_summary_v12_a2_t2.nondecisionMRI(idx_OA,1);
scatter(tmp_yEEG, tmp_yMRI, 'filled')
title('Reliability NDT L4-1')

%% plot intercept-change correlations of behavior

figure;
subplot(4,2,1);
scatter(STSWD_summary.behav.MRIAcc(:,1), STSWD_summary.behav.MRIAcc(:,4)-STSWD_summary.behav.MRIAcc(:,1), 'filled'); xlabel('MRI Acc L1'); ylabel('MRI Acc L4-1')
subplot(4,2,2);
scatter(STSWD_summary.behav.EEGAcc(:,1), STSWD_summary.behav.EEGAcc(:,4)-STSWD_summary.behav.EEGAcc(:,1), 'filled'); xlabel('EEG Acc L1'); ylabel('EEG Acc L4-1')
subplot(4,2,3);
scatter(STSWD_summary.behav.MRIRT(:,1), STSWD_summary.behav.MRIRT(:,4)-STSWD_summary.behav.MRIRT(:,1), 'filled'); xlabel('MRI RT L1'); ylabel('MRI RT L4-1')
subplot(4,2,4);
scatter(STSWD_summary.behav.EEGRT(:,1), STSWD_summary.behav.EEGRT(:,4)-STSWD_summary.behav.EEGRT(:,1), 'filled'); xlabel('EEG RT L1'); ylabel('EEG RT L4-1')
subplot(4,2,5);
scatter(STSWD_summary.behav.MRIAcc(:,1), STSWD_summary.behav.MRIRT(:,4)-STSWD_summary.behav.MRIRT(:,1), 'filled'); xlabel('MRI Acc L1'); ylabel('MRI RT L4-1')
subplot(4,2,6);
scatter(STSWD_summary.behav.EEGAcc(:,1), STSWD_summary.behav.EEGRT(:,4)-STSWD_summary.behav.EEGRT(:,1), 'filled'); xlabel('EEG Acc L1'); ylabel('EEG RT L4-1')
subplot(4,2,7);
scatter(STSWD_summary.behav.MRIRT(:,1), STSWD_summary.behav.MRIAcc(:,4)-STSWD_summary.behav.MRIAcc(:,1), 'filled'); xlabel('MRI RT L1'); ylabel('MRI Acc L4-1')
subplot(4,2,8);
scatter(STSWD_summary.behav.EEGRT(:,1), STSWD_summary.behav.EEGAcc(:,4)-STSWD_summary.behav.EEGAcc(:,1), 'filled'); xlabel('EEG RT L1'); ylabel('EEG Acc L4-1')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

% calculate relative change scores

figure;
subplot(4,2,1);
scatter(STSWD_summary.behav.MRIAcc(:,1), (STSWD_summary.behav.MRIAcc(:,4)-STSWD_summary.behav.MRIAcc(:,1))./STSWD_summary.behav.MRIAcc(:,1), 'filled'); xlabel('MRI Acc L1'); ylabel('MRI Acc L4-1, rel. change')
subplot(4,2,2);
scatter(STSWD_summary.behav.EEGAcc(:,1), (STSWD_summary.behav.EEGAcc(:,4)-STSWD_summary.behav.EEGAcc(:,1))./STSWD_summary.behav.EEGAcc(:,1), 'filled'); xlabel('EEG Acc L1'); ylabel('EEG Acc L4-1, rel. change')
subplot(4,2,3);
scatter(STSWD_summary.behav.MRIRT(:,1), (STSWD_summary.behav.MRIRT(:,4)-STSWD_summary.behav.MRIRT(:,1))./STSWD_summary.behav.MRIRT(:,1), 'filled'); xlabel('MRI RT L1'); ylabel('MRI RT L4-1, rel. change')
subplot(4,2,4);
scatter(STSWD_summary.behav.EEGRT(:,1), (STSWD_summary.behav.EEGRT(:,4)-STSWD_summary.behav.EEGRT(:,1))./STSWD_summary.behav.EEGRT(:,1), 'filled'); xlabel('EEG RT L1'); ylabel('EEG RT L4-1, rel. change')
subplot(4,2,5);
scatter(STSWD_summary.behav.MRIAcc(:,1), (STSWD_summary.behav.MRIRT(:,4)-STSWD_summary.behav.MRIRT(:,1))./STSWD_summary.behav.MRIRT(:,1), 'filled'); xlabel('MRI Acc L1'); ylabel('MRI RT L4-1, rel. change')
subplot(4,2,6);
scatter(STSWD_summary.behav.EEGAcc(:,1), (STSWD_summary.behav.EEGRT(:,4)-STSWD_summary.behav.EEGRT(:,1))./STSWD_summary.behav.EEGRT(:,1), 'filled'); xlabel('EEG Acc L1'); ylabel('EEG RT L4-1, rel. change')
subplot(4,2,7);
scatter(STSWD_summary.behav.MRIRT(:,1), (STSWD_summary.behav.MRIAcc(:,4)-STSWD_summary.behav.MRIAcc(:,1))./STSWD_summary.behav.MRIAcc(:,1), 'filled'); xlabel('MRI RT L1'); ylabel('MRI Acc L4-1, rel. change')
subplot(4,2,8);
scatter(STSWD_summary.behav.EEGRT(:,1), (STSWD_summary.behav.EEGAcc(:,4)-STSWD_summary.behav.EEGAcc(:,1))./STSWD_summary.behav.EEGAcc(:,1), 'filled'); xlabel('EEG RT L1'); ylabel('EEG Acc L4-1, rel. change')
set(findall(gcf,'-property','FontSize'),'FontSize',18)


figure;
subplot(4,2,1);
scatter(HDDM_summary_v12_a2_t2.nondecisionMRI(:,1), (HDDM_summary_v12_a2_t2.nondecisionMRI(:,4)-HDDM_summary_v12_a2_t2.nondecisionMRI(:,1))./HDDM_summary_v12_a2_t2.nondecisionMRI(:,1), 'filled'); xlabel('MRI NDT L1'); ylabel('MRI NDT L4-1, rel. change')
subplot(4,2,2);
scatter(HDDM_summary_v12_a2_t2.nondecisionEEG(:,1), (HDDM_summary_v12_a2_t2.nondecisionEEG(:,4)-HDDM_summary_v12_a2_t2.nondecisionEEG(:,1))./HDDM_summary_v12_a2_t2.nondecisionMRI(:,1), 'filled'); xlabel('EEG NDT L1'); ylabel('EEG NDT L4-1, rel. change')
subplot(4,2,3);
scatter(HDDM_summary_v12_a2_t2.driftMRI(:,1), (HDDM_summary_v12_a2_t2.driftMRI(:,4)-HDDM_summary_v12_a2_t2.driftMRI(:,1))./HDDM_summary_v12_a2_t2.driftMRI(:,1), 'filled'); xlabel('MRI Drift L1'); ylabel('MRI Drift L4-1, rel. change')
subplot(4,2,4);
scatter(HDDM_summary_v12_a2_t2.driftEEG(:,1), (HDDM_summary_v12_a2_t2.driftEEG(:,4)-HDDM_summary_v12_a2_t2.driftEEG(:,1))./HDDM_summary_v12_a2_t2.driftEEG(:,1), 'filled'); xlabel('EEG Drift L1'); ylabel('EEG Drift L4-1, rel. change')
subplot(4,2,5);
scatter(HDDM_summary_v12_a2_t2.nondecisionMRI(:,1), (HDDM_summary_v12_a2_t2.driftMRI(:,4)-HDDM_summary_v12_a2_t2.driftMRI(:,1))./HDDM_summary_v12_a2_t2.driftMRI(:,1), 'filled'); xlabel('MRI NDT L1'); ylabel('MRI Drift L4-1, rel. change')
subplot(4,2,6);
scatter(HDDM_summary_v12_a2_t2.driftMRI(:,1), (HDDM_summary_v12_a2_t2.nondecisionMRI(:,4)-HDDM_summary_v12_a2_t2.nondecisionMRI(:,1))./HDDM_summary_v12_a2_t2.nondecisionMRI(:,1), 'filled'); xlabel('MRI Drift L1'); ylabel('MRI NDT L4-1, rel. change')
subplot(4,2,7);
scatter(HDDM_summary_v12_a2_t2.nondecisionEEG(:,1), (HDDM_summary_v12_a2_t2.driftEEG(:,4)-HDDM_summary_v12_a2_t2.driftEEG(:,1))./HDDM_summary_v12_a2_t2.driftEEG(:,1), 'filled'); xlabel('EEG NDT L1'); ylabel('EEG Drift L4-1, rel. change')
subplot(4,2,8);
scatter(HDDM_summary_v12_a2_t2.driftEEG(:,1), (HDDM_summary_v12_a2_t2.nondecisionEEG(:,4)-HDDM_summary_v12_a2_t2.nondecisionEEG(:,1))./HDDM_summary_v12_a2_t2.nondecisionEEG(:,1), 'filled'); xlabel('EEG Drift L1'); ylabel('EEG NDT L4-1, rel. change')
set(findall(gcf,'-property','FontSize'),'FontSize',18)


figure;
subplot(4,2,1);
scatter(HDDM_summary_v12_a2_t2.nondecisionMRI(:,1), (STSWD_summary.behav.MRIAcc(idxSummary,4)-STSWD_summary.behav.MRIAcc(idxSummary,1))./STSWD_summary.behav.MRIAcc(idxSummary,1), 'filled'); xlabel('MRI NDT L1'); ylabel('MRI Acc L4-1, rel. change')
subplot(4,2,2);
subplot(4,2,3);
scatter(HDDM_summary_v12_a2_t2.driftMRI(:,1), (STSWD_summary.behav.MRIRT(idxSummary,4)-STSWD_summary.behav.MRIRT(idxSummary,1))./STSWD_summary.behav.MRIRT(idxSummary,1), 'filled'); xlabel('MRI Drift L1'); ylabel('MRI RT L4-1, rel. change')
subplot(4,2,4);
subplot(4,2,5);
scatter(HDDM_summary_v12_a2_t2.nondecisionMRI(:,1), (STSWD_summary.behav.MRIRT(idxSummary,4)-STSWD_summary.behav.MRIRT(idxSummary,1))./STSWD_summary.behav.MRIRT(idxSummary,1), 'filled'); xlabel('MRI NDT L1'); ylabel('MRI RT L4-1, rel. change')
subplot(4,2,6);
scatter(HDDM_summary_v12_a2_t2.driftMRI(:,1), (STSWD_summary.behav.MRIAcc(idxSummary,4)-STSWD_summary.behav.MRIAcc(idxSummary,1))./STSWD_summary.behav.MRIAcc(idxSummary,1), 'filled'); xlabel('MRI Drift L1'); ylabel('MRI Acc L4-1, rel. change')
subplot(4,2,7);
scatter(HDDM_summary_v12_a2_t2.driftMRI(:,1), STSWD_summary.behav.MRIAcc(idxSummary,1), 'filled'); xlabel('MRI Drift L1'); ylabel('MRI Acc L1')
subplot(4,2,8);
scatter((HDDM_summary_v12_a2_t2.nondecisionMRI(:,4)-HDDM_summary_v12_a2_t2.nondecisionMRI(:,1))./HDDM_summary_v12_a2_t2.nondecisionMRI(:,1), (STSWD_summary.behav.MRIRT(:,4)-STSWD_summary.behav.MRIRT(:,1))./STSWD_summary.behav.MRIRT(:,1), 'filled'); xlabel('MRI NDT L4-L1'); ylabel('MRI RT L4-1, rel. change')

set(findall(gcf,'-property','FontSize'),'FontSize',18)


figure;
scatter(HDDM_summary_v12_a2_t2.driftMRI(:,4), STSWD_summary.behav.MRIAcc(idxSummary,1), 'filled'); xlabel('MRI Drift L4-1'); ylabel('MRI Acc L4-1')
scatter(HDDM_summary_v12_a2_t2.driftMRI(:,4)-HDDM_summary_v12_a2_t2.driftMRI(:,1), (STSWD_summary.behav.MRIAcc(idxSummary,4)-STSWD_summary.behav.MRIAcc(idxSummary,1)), 'filled'); xlabel('MRI Drift L4-1'); ylabel('MRI Acc L4-1')
scatter(HDDM_summary_v12_a2_t2.driftMRI(:,4)-HDDM_summary_v12_a2_t2.driftMRI(:,1), (STSWD_summary.behav.MRIRT(idxSummary,4)-STSWD_summary.behav.MRIRT(idxSummary,1)), 'filled'); xlabel('MRI Drift L4-1'); ylabel('MRI Acc L4-1')
