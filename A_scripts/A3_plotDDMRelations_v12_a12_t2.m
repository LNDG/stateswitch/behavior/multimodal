%% Plot associations between multimodal data

dataPath = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/D_DDM/B_data/';
load([dataPath, 'HDDM_summary_v12_a12_t2.mat'], 'HDDM_summary_v12_a12_t2')

idx_YA = cellfun(@str2num, HDDM_summary_v12_a12_t2.IDs)<2000;
idx_OA = cellfun(@str2num, HDDM_summary_v12_a12_t2.IDs)>2000;

%% preliminary plots as sanity checks:

% LV1 and 234-1 change in ndtime

figure;
subplot(1,3,1);
xData = squeeze(nanmean(STSWD_summary.brainscoreSummary.DDM_LV1(:,2:4),2))-...
    squeeze(nanmean(STSWD_summary.brainscoreSummary.DDM_LV1(:,1),2));
yData = squeeze(nanmean(HDDM_summary_v12_a12_t2.nondecision_PCA(:,2:4),2))-...
    squeeze(nanmean(HDDM_summary_v12_a12_t2.nondecision_PCA(:,1),2));
scatter(xData, yData, 'k', 'filled'); title('LV1 - ND correlation 234-1')

% LV2 and 1234 threshold

subplot(1,3,2);
xData = squeeze(nanmean(STSWD_summary.brainscoreSummary.DDM_LV2(:,1:4),2));
yData = squeeze(nanmean(HDDM_summary_v12_a12_t2.nondecision_PCA(:,1:4),2));
scatter(xData, yData, 'k', 'filled');  title('LV2 - Threshold correlation condMean')

% LV3 and 1234 drift

subplot(1,3,3);
xData = squeeze(nanmean(STSWD_summary.brainscoreSummary.DDM_LV3(:,1:4),2));
yData = squeeze(nanmean(HDDM_summary_v12_a12_t2.drift_PCA(:,1:4),2));
scatter(xData, yData, 'k', 'filled');  title('LV3 - Drift correlation condMean')

%% LV1 and alpha power

% stronger alpha desynchronization with stronger thalamic power
% higher drift rate with stronger alpha desynchronization
% but only for YA! is this particular baselining harmful in OAs?

figure;
subplot(1,2,1); hold on;
    xData = squeeze(nanmean(STSWD_summary.brainscoreSummary.DDM_LV1(idx_YA,1:4),2));
    yData = squeeze(nanmean(STSWD_summary.TFR.stimAlpha(idx_YA,1:4),2));
    scatter(xData, yData, 'k', 'filled'); l1 = lsline;
    xData = squeeze(nanmean(STSWD_summary.brainscoreSummary.DDM_LV1(idx_OA,1:4),2));
    yData = squeeze(nanmean(STSWD_summary.TFR.stimAlpha(idx_OA,1:4),2));
    scatter(xData, yData, 'r', 'filled'); l2 = lsline;
    title('LV1 - stim alpha 1234')    
    xlabel('LV1 brainscore'); ylabel('Alpha Power (whole-trial baselined)')
subplot(1,2,2); hold on;
    xData = squeeze(nanmean(HDDM_summary_v12_a12_t2.drift_PCA(idx_YA,1:4),2));
    yData = squeeze(nanmean(STSWD_summary.TFR.stimAlpha(idx_YA,1:4),2));
    scatter(xData, yData, 'k', 'filled'); l1 = lsline;
    xData = squeeze(nanmean(HDDM_summary_v12_a12_t2.drift_PCA(idx_OA,1:4),2));
    yData = squeeze(nanmean(STSWD_summary.TFR.stimAlpha(idx_OA,1:4),2));
    scatter(xData, yData, 'r', 'filled'); l2 = lsline;
    title('stim alpha 1234')
    xlabel('Drift rate'); ylabel('Alpha Power (whole-trial baselined)')
    
figure;
xData = squeeze(nanmean(STSWD_summary.brainscoreSummary.DDM_LV1(:,4),2))-...
    squeeze(nanmean(STSWD_summary.brainscoreSummary.DDM_LV1(:,1),2));
yData = squeeze(nanmean(STSWD_summary.TFR.stimAlpha(:,4),2))-...
    squeeze(nanmean(STSWD_summary.TFR.stimAlpha(:,1),2));
scatter(xData, yData, 'k', 'filled'); title('LV1 - stim alpha 4-1')

figure;
xData = squeeze(nanmean(STSWD_summary.brainscoreSummary.DDM_LV1(idx_YA,4),2))-...
    squeeze(nanmean(STSWD_summary.brainscoreSummary.DDM_LV1(idx_YA,1),2));
yData = squeeze(nanmean(STSWD_summary.TFR.probeTheta(idx_YA,4),2))-...
    squeeze(nanmean(STSWD_summary.TFR.probeTheta(idx_YA,1),2));
scatter(xData, yData, 'k', 'filled'); title('LV1 - probe theta 4-1')

% probe theta and ND changes

figure;
xData = squeeze(nanmean(STSWD_summary.TFR.probeTheta(idx_YA,4),2))-...
    squeeze(nanmean(STSWD_summary.TFR.probeTheta(idx_YA,1),2));
yData = squeeze(nanmean(HDDM_summary_v12_a12_t2.nondecisionEEG(idx_YA,4),2))-...
    squeeze(nanmean(HDDM_summary_v12_a12_t2.nondecisionEEG(idx_YA,1),2));
scatter(xData, yData, 'k', 'filled'); title('thetaProbe - ND correlation 4-1')

% pupil during stim (4-1) -- alpha during stim (4-1)

figure; hold on;
xData = squeeze(nanmean(STSWD_summary.pupil.stimPupilZ(idx_YA,4),2))-...
    squeeze(nanmean(STSWD_summary.pupil.stimPupilZ(idx_YA,1),2));
yData = squeeze(nanmean(STSWD_summary.TFR.stimAlpha(idx_YA,4),2))-...
    squeeze(nanmean(STSWD_summary.TFR.stimAlpha(idx_YA,1),2));
scatter(xData, yData, 'k', 'filled'); l1 = lsline;
xData = squeeze(nanmean(STSWD_summary.pupil.stimPupilZ(idx_OA,4),2))-...
    squeeze(nanmean(STSWD_summary.pupil.stimPupilZ(idx_OA,1),2));
yData = squeeze(nanmean(STSWD_summary.TFR.stimAlpha(idx_OA,4),2))-...
    squeeze(nanmean(STSWD_summary.TFR.stimAlpha(idx_OA,1),2));
scatter(xData, yData, 'r', 'filled'); l2 = lsline;
title('pupil stim - stim alpha 1234')    
xlabel('pupil stim'); ylabel('Alpha Power (whole-trial baselined)')

figure; hold on;
xData = squeeze(nanmean(STSWD_summary.pupil.stimPupilRelChange(idx_YA,4),2))-...
    squeeze(nanmean(STSWD_summary.pupil.stimPupilRelChange(idx_YA,1),2));
yData = squeeze(nanmean(STSWD_summary.TFR.cueAlpha(idx_YA,4),2))-...
    squeeze(nanmean(STSWD_summary.TFR.cueAlpha(idx_YA,1),2));
scatter(xData, yData, 'k', 'filled'); l1 = lsline;
xData = squeeze(nanmean(STSWD_summary.pupil.stimPupilRelChange(idx_OA,4),2))-...
    squeeze(nanmean(STSWD_summary.pupil.stimPupilRelChange(idx_OA,1),2));
yData = squeeze(nanmean(STSWD_summary.TFR.cueAlpha(idx_OA,4),2))-...
    squeeze(nanmean(STSWD_summary.TFR.cueAlpha(idx_OA,1),2));
scatter(xData, yData, 'r', 'filled'); l2 = lsline;
title('pupil stim - stim alpha 1234')    
xlabel('pupil stim'); ylabel('Alpha Power (whole-trial baselined)')

%% gigantic cross-correlation matrix

MultiModalMatrix = [STSWD_summary.TFR.cueAlpha,...
    STSWD_summary.TFR.prestimAlpha, ...
    STSWD_summary.TFR.stimAlpha, ...
    STSWD_summary.TFR.probeAlpha, ...
    STSWD_summary.TFR.probeTheta, ...
    STSWD_summary.brainscoreSummary.meanTask,...
    STSWD_summary.brainscoreSummary.DDM_LV1,...
    STSWD_summary.brainscoreSummary.DDM_LV2,...
    STSWD_summary.brainscoreSummary.DDM_LV3,...
    STSWD_summary.brainscoreSummary.DDM_LV5,...
    STSWD_summary.behav.RT_PCA,...
    STSWD_summary.behav.Acc_PCA,...
    HDDM_summary_v12_a12_t2.threshold_PCA,...
    HDDM_summary_v12_a12_t2.nondecision_PCA,...
    HDDM_summary_v12_a12_t2.drift_PCA,...
    STSWD_summary.pupil.cuePupilRaw,... % strongly correlated with stimPupilRaw --> global differences
    STSWD_summary.pupil.stimPupilRaw,...
    STSWD_summary.pupil.cuePupilZ,...
    STSWD_summary.pupil.stimPupilZ,...
    STSWD_summary.pupil.cuePupilRelChange,...
    STSWD_summary.pupil.stimPupilRelChange];

MultiModalMatrix(MultiModalMatrix ==0) = NaN;

[CrossCorrRho, CrossCorrP] = corrcoef(MultiModalMatrix, 'rows', 'complete');
CrossCorrRho(CrossCorrP>.05) = 0;
figure; imagesc(CrossCorrRho)

%% 4-1 change

MultiModalLabel = {'cueAlpha'; 'prestimAlpha'; 'stimAlpha'; 'probeAlpha'; 'probeTheta'; ...
    'meanTaskLV'; 'LV1'; 'LV2'; 'LV3'; 'LV5'; 'RT'; 'ACC'; 'thresh'; 'NDT'; ...
    'drift'; 'cuePupilRaw'; 'stimPupilRaw'; 'cuePupilZ'; 'stimPupilZ'; ...
    'cuePupilRelChange'; 'stimPupilRelChange'};

MultiModalChangeMatrix = [STSWD_summary.TFR.cueAlpha(:,4)-STSWD_summary.TFR.cueAlpha(:,1),...
    STSWD_summary.TFR.prestimAlpha(:,4)-STSWD_summary.TFR.prestimAlpha(:,1), ...
    STSWD_summary.TFR.stimAlpha(:,4)-STSWD_summary.TFR.stimAlpha(:,1), ...
    STSWD_summary.TFR.probeAlpha(:,4)-STSWD_summary.TFR.probeAlpha(:,1), ...
    STSWD_summary.TFR.probeTheta(:,4)-STSWD_summary.TFR.probeTheta(:,1), ...
    STSWD_summary.brainscoreSummary.meanTask(:,4)-STSWD_summary.brainscoreSummary.meanTask(:,1),...
    STSWD_summary.brainscoreSummary.DDM_LV1(:,4)-STSWD_summary.brainscoreSummary.DDM_LV1(:,1),...
    STSWD_summary.brainscoreSummary.DDM_LV2(:,4)-STSWD_summary.brainscoreSummary.DDM_LV2(:,1),...
    STSWD_summary.brainscoreSummary.DDM_LV3(:,4)-STSWD_summary.brainscoreSummary.DDM_LV3(:,1),...
    STSWD_summary.brainscoreSummary.DDM_LV5(:,4)-STSWD_summary.brainscoreSummary.DDM_LV5(:,1),...
    STSWD_summary.behav.RT_PCA(:,4)-STSWD_summary.behav.RT_PCA(:,1),...
    STSWD_summary.behav.Acc_PCA(:,4)-STSWD_summary.behav.Acc_PCA(:,1),...
    HDDM_summary_v12_a12_t2.threshold_PCA(:,4)-HDDM_summary_v12_a12_t2.threshold_PCA(:,1),...
    HDDM_summary_v12_a12_t2.nondecision_PCA(:,4)-HDDM_summary_v12_a12_t2.nondecision_PCA(:,1),...
    HDDM_summary_v12_a12_t2.drift_PCA(:,4)-HDDM_summary_v12_a12_t2.drift_PCA(:,1),...
    STSWD_summary.pupil.cuePupilRaw(:,4)-STSWD_summary.pupil.cuePupilRaw(:,1),...
    STSWD_summary.pupil.stimPupilRaw(:,4)-STSWD_summary.pupil.stimPupilRaw(:,1),...
    STSWD_summary.pupil.cuePupilZ(:,4)-STSWD_summary.pupil.cuePupilZ(:,1),...
    STSWD_summary.pupil.stimPupilZ(:,4)-STSWD_summary.pupil.stimPupilZ(:,1),...
    STSWD_summary.pupil.cuePupilRelChange(:,4)-STSWD_summary.pupil.cuePupilRelChange(:,1),...
    STSWD_summary.pupil.stimPupilRelChange(:,4)-STSWD_summary.pupil.stimPupilRelChange(:,1)];

MultiModalChangeMatrix(MultiModalChangeMatrix ==0) = NaN;

[CrossCorrRho, CrossCorrP] = corrcoef(MultiModalChangeMatrix, 'rows', 'complete');
CrossCorrRho(CrossCorrP>.05) = 0;
figure; imagesc(CrossCorrRho)

% plot pairwise comparisons of interest

PairsOfInterest = [3,4; 7,9; 8,10; 9, 10; 11, 13; 11, 14; 12, 15; 13, 15; 16, 18; 16, 21];

h = figure('units','normalized','position',[.1 .1 .7 .7]);
for indPair = 1:10
    subplot(2,5,indPair); hold on;
    scatter(MultiModalChangeMatrix(idx_YA,PairsOfInterest(indPair,1)), MultiModalChangeMatrix(idx_YA,PairsOfInterest(indPair,2)), 'k', 'filled');
    scatter(MultiModalChangeMatrix(idx_OA,PairsOfInterest(indPair,1)), MultiModalChangeMatrix(idx_OA,PairsOfInterest(indPair,2)), 'r', 'filled');
    ylabel(MultiModalLabel{PairsOfInterest(indPair,1)}); xlabel(MultiModalLabel{PairsOfInterest(indPair,2)})
end
legend({'YA'; 'OA'});
set(findall(gcf,'-property','FontSize'),'FontSize',18)
suptitle('Dim4 - Dim1 correlations')

PairsOfInterest = [3,4; 3,11; 3,14; 3,17; 4, 6; 4,7];

h = figure('units','normalized','position',[.1 .1 .7 .7]);
for indPair = 1:size(PairsOfInterest,1)
    subplot(2,3,indPair); hold on;
    scatter(MultiModalChangeMatrix(idx_YA,PairsOfInterest(indPair,1)), MultiModalChangeMatrix(idx_YA,PairsOfInterest(indPair,2)), 'k', 'filled');
    scatter(MultiModalChangeMatrix(idx_OA,PairsOfInterest(indPair,1)), MultiModalChangeMatrix(idx_OA,PairsOfInterest(indPair,2)), 'r', 'filled');
    ylabel(MultiModalLabel{PairsOfInterest(indPair,1)}); xlabel(MultiModalLabel{PairsOfInterest(indPair,2)})
end
set(findall(gcf,'-property','FontSize'),'FontSize',18)

%% residual 4

MultiModalLabel = {'TFR.cueAlpha'; 'TFR.prestimAlpha'; 'TFR.stimAlpha'; 'TFR.probeAlpha'; 'TFR.probeTheta'; ...
    'behav.RT_PCA'; 'behav.Acc_PCA'; 'pupil.cuePupilRaw'; 'pupil.stimPupilRaw'; 'pupil.cuePupilZ'; 'pupil.stimPupilZ'; ...
    'pupil.cuePupilRelChange'; 'pupil.stimPupilRelChange'};

MultiModalChangeMatrix = [];
for indMeasure = 1:numel(MultiModalLabel)
    curData = eval(['STSWD_summary.',MultiModalLabel{indMeasure}]);
    [~, ~, residual] = regress(curData(:,4), curData(:,1));
    MultiModalChangeMatrix(:,indMeasure) = residual;
end

MultiModalChangeMatrix(MultiModalChangeMatrix ==0) = NaN;

[CrossCorrRho, CrossCorrP] = corrcoef(MultiModalChangeMatrix, 'rows', 'complete');
CrossCorrRho(CrossCorrP>.05) = 0;
figure; imagesc(CrossCorrRho)

figure;
scatter(MultiModalChangeMatrix(:,7),MultiModalChangeMatrix(:,3))

%% load 1 only

MultiModalLabel = {'cueAlpha'; 'prestimAlpha'; 'stimAlpha'; 'probeAlpha'; 'probeTheta'; ...
    'meanTaskLV'; 'LV1'; 'LV2'; 'LV3'; 'LV5'; 'RT'; 'ACC'; 'thresh'; 'NDT'; ...
    'drift'; 'cuePupilRaw'; 'stimPupilRaw'; 'cuePupilZ'; 'stimPupilZ'; ...
    'cuePupilRelChange'; 'stimPupilRelChange'};

MultiModalMatrix = [STSWD_summary.TFR.cueAlpha(idx_YA,1),...
    STSWD_summary.TFR.prestimAlpha(idx_YA,1), ...
    STSWD_summary.TFR.stimAlpha(idx_YA,1), ...
    STSWD_summary.TFR.probeAlpha(idx_YA,1), ...
    STSWD_summary.TFR.probeTheta(idx_YA,1), ...
    STSWD_summary.brainscoreSummary.meanTask(idx_YA,1),...
    STSWD_summary.brainscoreSummary.DDM_LV1(idx_YA,1),...
    STSWD_summary.brainscoreSummary.DDM_LV2(idx_YA,1),...
    STSWD_summary.brainscoreSummary.DDM_LV3(idx_YA,1),...
    STSWD_summary.brainscoreSummary.DDM_LV5(idx_YA,1),...
    STSWD_summary.behav.RT_PCA(idx_YA,1),...
    STSWD_summary.behav.Acc_PCA(idx_YA,1),...
    HDDM_summary_v12_a12_t2.threshold_PCA(idx_YA,1),...
    HDDM_summary_v12_a12_t2.nondecision_PCA(idx_YA,1),...
    HDDM_summary_v12_a12_t2.drift_PCA(idx_YA,1),...
    STSWD_summary.pupil.cuePupilRaw(idx_YA,1),...
    STSWD_summary.pupil.stimPupilRaw(idx_YA,1),...
    STSWD_summary.pupil.cuePupilZ(idx_YA,1),...
    STSWD_summary.pupil.stimPupilZ(idx_YA,1),...
    STSWD_summary.pupil.cuePupilRelChange(idx_YA,1),...
    STSWD_summary.pupil.stimPupilRelChange(idx_YA,1)];

MultiModalMatrix(MultiModalMatrix ==0) = NaN;

[CrossCorrRho, CrossCorrP] = corrcoef(MultiModalMatrix, 'rows', 'complete');
CrossCorrRho(CrossCorrP>.05) = 0;
figure; imagesc(CrossCorrRho)

% plot pairwise comparisons of interest

PairsOfInterest = [2,7; 2,8; 2,13; 3,4; 3, 6; 3,18];

h = figure('units','normalized','position',[.1 .1 .7 .7]);
for indPair = 1:size(PairsOfInterest,1)
    subplot(2,3,indPair); hold on;
    scatter(MultiModalChangeMatrix(idx_YA,PairsOfInterest(indPair,1)), MultiModalChangeMatrix(idx_YA,PairsOfInterest(indPair,2)), 'k', 'filled');
    scatter(MultiModalChangeMatrix(idx_OA,PairsOfInterest(indPair,1)), MultiModalChangeMatrix(idx_OA,PairsOfInterest(indPair,2)), 'r', 'filled');
    ylabel(MultiModalLabel{PairsOfInterest(indPair,1)}); xlabel(MultiModalLabel{PairsOfInterest(indPair,2)})
end
set(findall(gcf,'-property','FontSize'),'FontSize',18)

%% 4-2 change

MultiModalLabel = {'cueAlpha'; 'prestimAlpha'; 'stimAlpha'; 'probeAlpha'; 'probeTheta'; ...
    'meanTaskLV'; 'LV1'; 'LV2'; 'LV3'; 'LV5'; 'RT'; 'ACC'; 'thresh'; 'NDT'; ...
    'drift'; 'cuePupilRaw'; 'stimPupilRaw'; 'cuePupilZ'; 'stimPupilZ'; ...
    'cuePupilRelChange'; 'stimPupilRelChange'};

MultiModalChangeMatrix = [STSWD_summary.TFR.cueAlpha(:,4)-STSWD_summary.TFR.cueAlpha(:,2),...
    STSWD_summary.TFR.prestimAlpha(:,4)-STSWD_summary.TFR.prestimAlpha(:,2), ...
    STSWD_summary.TFR.stimAlpha(:,4)-STSWD_summary.TFR.stimAlpha(:,2), ...
    STSWD_summary.TFR.probeAlpha(:,4)-STSWD_summary.TFR.probeAlpha(:,2), ...
    STSWD_summary.TFR.probeTheta(:,4)-STSWD_summary.TFR.probeTheta(:,2), ...
    STSWD_summary.brainscoreSummary.meanTask(:,4)-STSWD_summary.brainscoreSummary.meanTask(:,2),...
    STSWD_summary.brainscoreSummary.DDM_LV1(:,5),...
    STSWD_summary.brainscoreSummary.DDM_LV2(:,5),...
    STSWD_summary.brainscoreSummary.DDM_LV3(:,5),...
    STSWD_summary.brainscoreSummary.DDM_LV5(:,5),...
    STSWD_summary.behav.RT_PCA(:,4)-STSWD_summary.behav.RT_PCA(:,2),...
    STSWD_summary.behav.Acc_PCA(:,4)-STSWD_summary.behav.Acc_PCA(:,2),...
    HDDM_summary_v12_a12_t2.threshold_PCA(:,4)-HDDM_summary_v12_a12_t2.threshold_PCA(:,2),...
    HDDM_summary_v12_a12_t2.nondecision_PCA(:,4)-HDDM_summary_v12_a12_t2.nondecision_PCA(:,2),...
    HDDM_summary_v12_a12_t2.drift_PCA(:,4)-HDDM_summary_v12_a12_t2.drift_PCA(:,2),...
    STSWD_summary.pupil.cuePupilRaw(:,4)-STSWD_summary.pupil.cuePupilRaw(:,2),...
    STSWD_summary.pupil.stimPupilRaw(:,4)-STSWD_summary.pupil.stimPupilRaw(:,2),...
    STSWD_summary.pupil.cuePupilZ(:,4)-STSWD_summary.pupil.cuePupilZ(:,2),...
    STSWD_summary.pupil.stimPupilZ(:,4)-STSWD_summary.pupil.stimPupilZ(:,2),...
    STSWD_summary.pupil.cuePupilRelChange(:,4)-STSWD_summary.pupil.cuePupilRelChange(:,2),...
    STSWD_summary.pupil.stimPupilRelChange(:,4)-STSWD_summary.pupil.stimPupilRelChange(:,2)];

MultiModalChangeMatrix(MultiModalChangeMatrix ==0) = NaN;

[CrossCorrRho, CrossCorrP] = corrcoef(MultiModalChangeMatrix, 'rows', 'complete');
CrossCorrRho(CrossCorrP>.05) = 0;
figure; imagesc(CrossCorrRho)

figure;
hold on;
NaNIDs = find(isnan(MultiModalChangeMatrix(idx_YA,3)) | isnan(MultiModalChangeMatrix(idx_YA,19)));
idx_YA(NaNIDs) = 0;
scatter(MultiModalChangeMatrix(idx_YA,3), MultiModalChangeMatrix(idx_YA,19), 'k', 'filled'); lsline();
corrcoef(MultiModalChangeMatrix(idx_YA,3), MultiModalChangeMatrix(idx_YA,19))
ylabel(MultiModalLabel{3}); xlabel(MultiModalLabel{19})
title('Young adults');
NaNIDs = find(isnan(MultiModalChangeMatrix(idx_OA,3)) | isnan(MultiModalChangeMatrix(idx_OA,19)));
idx_OA(NaNIDs) = 0;
scatter(MultiModalChangeMatrix(idx_OA,3), MultiModalChangeMatrix(idx_OA,19), 'r', 'filled'); lsline();
corrcoef(MultiModalChangeMatrix(idx_OA,3), MultiModalChangeMatrix(idx_OA,19))
ylabel(MultiModalLabel{3}); xlabel(MultiModalLabel{19})
title('Older adults');

%% DDM comparison

figure;
imagesc(corrcoef([HDDM_summary_v12_a12_t2.thresholdMRI, HDDM_summary_v12_a12_t2.thresholdMRI], 'rows', 'complete'))
imagesc(corrcoef([HDDM_summary_v12_a12_t2.thresholdMRI, HDDM_summary_v12_a12_t2.thresholdEEG], 'rows', 'complete'))
imagesc(corrcoef([HDDM_summary_v12_a12_t2.thresholdMRI, HDDM_summary_v12_a12_t2.thresholdEEG], 'rows', 'complete'))
imagesc(corrcoef([HDDM_summary_v12_a12_t2.thresholdMRI, HDDM_summary_v12_a12_t2.thresholdMRI], 'rows', 'complete'))

figure; hold on; scatter(reshape(HDDM_summary_v12_a12_t2.thresholdMRI,1,[]), reshape(HDDM_summary_v12_a12_t2.thresholdEEG,1,[]), 'filled')
hold on; scatter(reshape(HDDM_summary_v12_a12_t2.driftMRI,1,[]), reshape(HDDM_summary_v12_a12_t2.driftEEG,1,[]), 'filled')
hold on; scatter(reshape(HDDM_summary_v12_a12_t2.nondecisionMRI,1,[]), reshape(HDDM_summary_v12_a12_t2.nondecisionEEG,1,[]), 'filled')

figure; hold on; scatter(reshape(HDDM_summary_v12_a12_t2.thresholdMRI,1,[]), reshape(HDDM_summary_v12_a12_t2.thresholdEEG,1,[]), 'filled')
hold on; scatter(reshape(HDDM_summary_v12_a12_t2.driftMRI,1,[]), reshape(HDDM_summary_v12_a12_t2.driftEEG,1,[]), 'filled')
hold on; scatter(reshape(HDDM_summary_v12_a12_t2.nondecisionMRI,1,[]), reshape(HDDM_summary_v12_a12_t2.nondecisionEEG,1,[]), 'filled')

% compare HDDM with DDM: YA
figure; hold on; scatter(reshape(HDDM_summary_v12_a12_t2.thresholdEEG(idx_YA,:),1,[]), reshape(HDDM_summary_v12_a12_t2.thresholdEEG(idx_YA,:),1,[]), 'filled')
hold on; scatter(reshape(HDDM_summary_v12_a12_t2.driftEEG(idx_YA,:),1,[]), reshape(HDDM_summary_v12_a12_t2.driftEEG(idx_YA,:),1,[]), 'filled')
hold on; scatter(reshape(HDDM_summary_v12_a12_t2.nondecisionEEG(idx_YA,:),1,[]), reshape(HDDM_summary_v12_a12_t2.nondecisionEEG(idx_YA,:),1,[]), 'filled')

figure; 

subplot(3,2,1); hold on; l1 = scatter(reshape(HDDM_summary_v12_a12_t2.thresholdMRI(idx_YA,4)-HDDM_summary_v12_a12_t2.thresholdMRI(idx_YA,1),1,[]), ...
    reshape(HDDM_summary_v12_a12_t2.thresholdEEG(idx_YA,4)-HDDM_summary_v12_a12_t2.thresholdEEG(idx_YA,1),1,[]), 'filled'); lsline;
hold on; l2 = scatter(reshape(HDDM_summary_v12_a12_t2.driftMRI(idx_YA,4)-HDDM_summary_v12_a12_t2.driftMRI(idx_YA,1),1,[]), ...
    reshape(HDDM_summary_v12_a12_t2.driftEEG(idx_YA,4)-HDDM_summary_v12_a12_t2.driftEEG(idx_YA,1),1,[]), 'filled'); l2 = lsline;
hold on; l3 = scatter(reshape(HDDM_summary_v12_a12_t2.nondecisionMRI(idx_YA,4)-HDDM_summary_v12_a12_t2.nondecisionMRI(idx_YA,1),1,[]), ...
    reshape(HDDM_summary_v12_a12_t2.nondecisionEEG(idx_YA,4)-HDDM_summary_v12_a12_t2.nondecisionEEG(idx_YA,1),1,[]), 'filled'); l3 = lsline;
legend([l1, l2, l3], {'Threshold Change'; 'Drift Change'; 'NDT Change'}, 'location', 'NorthWest');
title('4-1 Reliability DDM YA')

subplot(3,2,2); hold on; scatter(reshape(HDDM_summary_v12_a12_t2.thresholdMRI(idx_OA,4)-HDDM_summary_v12_a12_t2.thresholdMRI(idx_OA,1),1,[]), ...
    reshape(HDDM_summary_v12_a12_t2.thresholdEEG(idx_OA,4)-HDDM_summary_v12_a12_t2.thresholdEEG(idx_OA,1),1,[]), 'filled'); l1 = lsline;
hold on; scatter(reshape(HDDM_summary_v12_a12_t2.driftMRI(idx_OA,4)-HDDM_summary_v12_a12_t2.driftMRI(idx_OA,1),1,[]), ...
    reshape(HDDM_summary_v12_a12_t2.driftEEG(idx_OA,4)-HDDM_summary_v12_a12_t2.driftEEG(idx_OA,1),1,[]), 'filled'); l2 = lsline;
hold on; scatter(reshape(HDDM_summary_v12_a12_t2.nondecisionMRI(idx_OA,4)-HDDM_summary_v12_a12_t2.nondecisionMRI(idx_OA,1),1,[]), ...
    reshape(HDDM_summary_v12_a12_t2.nondecisionEEG(idx_OA,4)-HDDM_summary_v12_a12_t2.nondecisionEEG(idx_OA,1),1,[]), 'filled'); l3 = lsline;
legend([l1, l2, l3], {'Threshold Change'; 'Drift Change'; 'NDT Change'}, 'location', 'NorthWest');
title('4-1 Reliability DDM OA')

subplot(3,2,3); hold on; scatter(reshape(HDDM_summary_v12_a12_t2.thresholdMRI(idx_YA,4)-HDDM_summary_v12_a12_t2.thresholdMRI(idx_YA,1),1,[]), ...
    reshape(HDDM_summary_v12_a12_t2.thresholdEEG(idx_YA,4)-HDDM_summary_v12_a12_t2.thresholdEEG(idx_YA,1),1,[]), 'filled'); l1 = lsline;
hold on; scatter(reshape(HDDM_summary_v12_a12_t2.driftMRI(idx_YA,4)-HDDM_summary_v12_a12_t2.driftMRI(idx_YA,1),1,[]), ...
    reshape(HDDM_summary_v12_a12_t2.driftEEG(idx_YA,4)-HDDM_summary_v12_a12_t2.driftEEG(idx_YA,1),1,[]), 'filled'); l2 = lsline;
hold on; scatter(reshape(HDDM_summary_v12_a12_t2.nondecisionMRI(idx_YA,4)-HDDM_summary_v12_a12_t2.nondecisionMRI(idx_YA,1),1,[]), ...
    reshape(HDDM_summary_v12_a12_t2.nondecisionEEG(idx_YA,4)-HDDM_summary_v12_a12_t2.nondecisionEEG(idx_YA,1),1,[]), 'filled'); l3 = lsline;
legend([l1, l2, l3], {'Threshold Change'; 'Drift Change'; 'NDT Change'}, 'location', 'NorthWest');
title('4-1 Reliability HDDM YA')

subplot(3,2,4); hold on; scatter(reshape(HDDM_summary_v12_a12_t2.thresholdMRI(idx_OA,4)-HDDM_summary_v12_a12_t2.thresholdMRI(idx_OA,1),1,[]), ...
    reshape(HDDM_summary_v12_a12_t2.thresholdEEG(idx_OA,4)-HDDM_summary_v12_a12_t2.thresholdEEG(idx_OA,1),1,[]), 'filled'); l1 = lsline;
hold on; scatter(reshape(HDDM_summary_v12_a12_t2.driftMRI(idx_OA,4)-HDDM_summary_v12_a12_t2.driftMRI(idx_OA,1),1,[]), ...
    reshape(HDDM_summary_v12_a12_t2.driftEEG(idx_OA,4)-HDDM_summary_v12_a12_t2.driftEEG(idx_OA,1),1,[]), 'filled'); l2 = lsline;
hold on; scatter(reshape(HDDM_summary_v12_a12_t2.nondecisionMRI(idx_OA,4)-HDDM_summary_v12_a12_t2.nondecisionMRI(idx_OA,1),1,[]), ...
    reshape(HDDM_summary_v12_a12_t2.nondecisionEEG(idx_OA,4)-HDDM_summary_v12_a12_t2.nondecisionEEG(idx_OA,1),1,[]), 'filled'); l3 = lsline;
legend([l1, l2, l3], {'Threshold Change'; 'Drift Change'; 'NDT Change'}, 'location', 'NorthWest');
title('4-1 Reliability HDDM OA')

subplot(3,2,5); hold on; scatter(reshape(HDDM_summary_v12_a12_t2wSess.thresholdMRI(idx_YA,4)-HDDM_summary_v12_a12_t2wSess.thresholdMRI(idx_YA,1),1,[]), ...
    reshape(HDDM_summary_v12_a12_t2wSess.thresholdEEG(idx_YA,4)-HDDM_summary_v12_a12_t2wSess.thresholdEEG(idx_YA,1),1,[]), 'filled'); l1 = lsline;
hold on; scatter(reshape(HDDM_summary_v12_a12_t2wSess.driftMRI(idx_YA,4)-HDDM_summary_v12_a12_t2wSess.driftMRI(idx_YA,1),1,[]), ...
    reshape(HDDM_summary_v12_a12_t2wSess.driftEEG(idx_YA,4)-HDDM_summary_v12_a12_t2.driftEEG(idx_YA,1),1,[]), 'filled'); l2 = lsline;
hold on; scatter(reshape(HDDM_summary_v12_a12_t2wSess.nondecisionMRI(idx_YA,4)-HDDM_summary_v12_a12_t2wSess.nondecisionMRI(idx_YA,1),1,[]), ...
    reshape(HDDM_summary_v12_a12_t2wSess.nondecisionEEG(idx_YA,4)-HDDM_summary_v12_a12_t2wSess.nondecisionEEG(idx_YA,1),1,[]), 'filled'); l3 = lsline;
legend([l1, l2, l3], {'Threshold Change'; 'Drift Change'; 'NDT Change'}, 'location', 'NorthWest');
title('4-1 Reliability HDDM w Session YA')

subplot(3,2,6); hold on; scatter(reshape(HDDM_summary_v12_a12_t2wSess.thresholdMRI(idx_OA,4)-HDDM_summary_v12_a12_t2wSess.thresholdMRI(idx_OA,1),1,[]), ...
    reshape(HDDM_summary_v12_a12_t2wSess.thresholdEEG(idx_OA,4)-HDDM_summary_v12_a12_t2wSess.thresholdEEG(idx_OA,1),1,[]), 'filled'); l1 = lsline;
hold on; scatter(reshape(HDDM_summary_v12_a12_t2wSess.driftMRI(idx_OA,4)-HDDM_summary_v12_a12_t2wSess.driftMRI(idx_OA,1),1,[]), ...
    reshape(HDDM_summary_v12_a12_t2wSess.driftEEG(idx_OA,4)-HDDM_summary_v12_a12_t2wSess.driftEEG(idx_OA,1),1,[]), 'filled'); l2 = lsline;
hold on; scatter(reshape(HDDM_summary_v12_a12_t2wSess.nondecisionMRI(idx_OA,4)-HDDM_summary_v12_a12_t2wSess.nondecisionMRI(idx_OA,1),1,[]), ...
    reshape(HDDM_summary_v12_a12_t2wSess.nondecisionEEG(idx_OA,4)-HDDM_summary_v12_a12_t2wSess.nondecisionEEG(idx_OA,1),1,[]), 'filled'); l3 = lsline;
legend([l1, l2, l3], {'Threshold Change'; 'Drift Change'; 'NDT Change'}, 'location', 'NorthWest');
title('4-1 Reliability HDDM w Session OA')

%% plot HDDM parameters by load & age

% threshold doesn't appear to strongly vary by age, maybe drop via model selection?

figure; 
subplot(2,2,1); bar(nanmean(HDDM_summary_v12_a12_t2.nondecisionEEG(idx_YA,:),1)); title('YA EEG'); xlabel('Load')
subplot(2,2,2); bar(nanmean(HDDM_summary_v12_a12_t2.nondecisionEEG(idx_OA,:),1)); title('OA EEG'); xlabel('Load')
subplot(2,2,3); bar(nanmean(HDDM_summary_v12_a12_t2.nondecisionMRI(idx_YA,:),1)); title('YA MRI'); xlabel('Load')
subplot(2,2,4); bar(nanmean(HDDM_summary_v12_a12_t2.nondecisionMRI(idx_OA,:),1)); title('OA MRI'); xlabel('Load')
suptitle('HDDM NDT')

figure; 
subplot(2,2,1); bar(nanmean(HDDM_summary_v12_a12_t2.thresholdEEG(idx_YA,:),1)); title('YA EEG'); xlabel('Load')
subplot(2,2,2); bar(nanmean(HDDM_summary_v12_a12_t2.thresholdEEG(idx_OA,:),1)); title('OA EEG'); xlabel('Load')
subplot(2,2,3); bar(nanmean(HDDM_summary_v12_a12_t2.thresholdMRI(idx_YA,:),1)); title('YA MRI'); xlabel('Load')
subplot(2,2,4); bar(nanmean(HDDM_summary_v12_a12_t2.thresholdMRI(idx_OA,:),1)); title('OA MRI'); xlabel('Load')
suptitle('HDDM Threshold')

figure; 
subplot(2,2,1); bar(nanmean(HDDM_summary_v12_a12_t2.driftEEG(idx_YA,:),1)); title('YA EEG'); xlabel('Load')
subplot(2,2,2); bar(nanmean(HDDM_summary_v12_a12_t2.driftEEG(idx_OA,:),1)); title('OA EEG'); xlabel('Load')
subplot(2,2,3); bar(nanmean(HDDM_summary_v12_a12_t2.driftMRI(idx_YA,:),1)); title('YA MRI'); xlabel('Load')
subplot(2,2,4); bar(nanmean(HDDM_summary_v12_a12_t2.driftMRI(idx_OA,:),1)); title('OA MRI'); xlabel('Load')
suptitle('HDDM Drift')

%% plot DDM parameters by load & age

% figure; 
% subplot(3,4,1); bar(nanmean(HDDM_summary_v12_a12_t2.nondecisionEEG(idx_YA,:),1)); ylim([0 .5]); title('NDT: YA EEG'); xlabel('Load')
% subplot(3,4,2); bar(nanmean(HDDM_summary_v12_a12_t2.nondecisionEEG(idx_OA,:),1)); ylim([0 .5]); title('NDT: OA EEG'); xlabel('Load')
% subplot(3,4,3); bar(nanmean(HDDM_summary_v12_a12_t2.nondecisionMRI(idx_YA,:),1)); ylim([0 .5]); title('NDT: YA MRI'); xlabel('Load')
% subplot(3,4,4); bar(nanmean(HDDM_summary_v12_a12_t2.nondecisionMRI(idx_OA,:),1)); ylim([0 .5]); title('NDT: OA MRI'); xlabel('Load')
% 
% subplot(3,4,4+1); bar(nanmean(HDDM_summary_v12_a12_t2.thresholdEEG(idx_YA,:),1)); ylim([0 1.5]); title('Threshold: YA EEG'); xlabel('Load')
% subplot(3,4,4+2); bar(nanmean(HDDM_summary_v12_a12_t2.thresholdEEG(idx_OA,:),1), [0 1.5]); title('Threshold: OA EEG'); xlabel('Load')
% subplot(3,4,4+3); bar(nanmean(HDDM_summary_v12_a12_t2.thresholdMRI(idx_YA,:),1), [0 1.5]); title('Threshold: YA MRI'); xlabel('Load')
% subplot(3,4,4+4); bar(nanmean(HDDM_summary_v12_a12_t2.thresholdMRI(idx_OA,:),1), [0 1.5]); title('Threshold: OA MRI'); xlabel('Load')
% 
% subplot(3,4,8+1); bar(nanmean(HDDM_summary_v12_a12_t2.driftEEG(idx_YA,:),1), [0 2]); title('Drift: YA EEG'); xlabel('Load')
% subplot(3,4,8+2); bar(nanmean(HDDM_summary_v12_a12_t2.driftEEG(idx_OA,:),1), [0 2]); title('Drift: OA EEG'); xlabel('Load')
% subplot(3,4,8+3); bar(nanmean(HDDM_summary_v12_a12_t2.driftMRI(idx_YA,:),1), [0 2]); title('Drift: YA MRI'); xlabel('Load')
% subplot(3,4,8+4); bar(nanmean(HDDM_summary_v12_a12_t2.driftMRI(idx_OA,:),1), [0 2]); title('Drift: OA MRI'); xlabel('Load')

%% plot slope reliability (change 1-2, 2-3, 3-4)

figure;
subplot(2,2,1);
tmp_yEEG = HDDM_summary_v12_a12_t2.thresholdEEG(idx_YA,1);
tmp_yMRI = HDDM_summary_v12_a12_t2.thresholdMRI(idx_YA,1);
scatter(tmp_yEEG, tmp_yMRI)
title('Reliability threshold L1')
subplot(2,2,2);
tmp_yEEG = HDDM_summary_v12_a12_t2.driftEEG(idx_OA,1);
tmp_yMRI = HDDM_summary_v12_a12_t2.driftMRI(idx_OA,1);
scatter(tmp_yEEG, tmp_yMRI)
title('Reliability drift L1')

subplot(2,2,3);
tmp_yEEG = HDDM_summary_v12_a12_t2.thresholdEEG(idx_YA,4)-HDDM_summary_v12_a12_t2.thresholdEEG(idx_YA,1);
tmp_yMRI = HDDM_summary_v12_a12_t2.thresholdMRI(idx_YA,4)-HDDM_summary_v12_a12_t2.thresholdMRI(idx_YA,1);
scatter(tmp_yEEG, tmp_yMRI)
title('Reliability threshold L4-1')
subplot(2,2,4);
tmp_yEEG = HDDM_summary_v12_a12_t2.driftEEG(idx_OA,4)-HDDM_summary_v12_a12_t2.driftEEG(idx_OA,1);
tmp_yMRI = HDDM_summary_v12_a12_t2.driftMRI(idx_OA,4)-HDDM_summary_v12_a12_t2.driftMRI(idx_OA,1);
scatter(tmp_yEEG, tmp_yMRI)
title('Reliability drift L4-1')


figure
tmp_yEEG = HDDM_summary_v12_a12_t2.nondecisionEEG(idx_YA,1);
tmp_yMRI = HDDM_summary_v12_a12_t2.nondecisionMRI(idx_YA,1);
scatter(tmp_yEEG, tmp_yMRI)
title('Reliability NDT L1')

%% plot LV values by load & age

figure; 
subplot(5,2,1); bar(nanmean(STSWD_summary.brainscoreSummary.meanTask(idx_YA,1:4),1)); ylim([-15 0]); title('YA meanTask'); xlabel('Load')
subplot(5,2,3); bar(nanmean(STSWD_summary.brainscoreSummary.DDM_LV1(idx_YA,5),1)); ylim([0 1.5]); title('YA DDM LV1'); xlabel('Load')
subplot(5,2,5); bar(nanmean(STSWD_summary.brainscoreSummary.DDM_LV2(idx_YA,1:4),1)); ylim([-6 0]); title('YA DDM LV2'); xlabel('Load')
subplot(5,2,7); bar(nanmean(STSWD_summary.brainscoreSummary.DDM_LV3(idx_YA,1:4),1)); ylim([0 10]); title('YA DDM LV3'); xlabel('Load')
subplot(5,2,9); bar(nanmean(STSWD_summary.brainscoreSummary.DDM_LV5(idx_YA,1:4),1)); ylim([0 5]); title('YA DDM LV5'); xlabel('Load')
subplot(5,2,2); bar(nanmean(STSWD_summary.brainscoreSummary.meanTask(idx_OA,1:4),1)); ylim([-15 0]); title('OA meanTask'); xlabel('Load')
subplot(5,2,4); bar(nanmean(STSWD_summary.brainscoreSummary.DDM_LV1(idx_OA,5),1)); ylim([0 1.5]); title('OA DDM LV1'); xlabel('Load')
subplot(5,2,6); bar(nanmean(STSWD_summary.brainscoreSummary.DDM_LV2(idx_OA,1:4),1)); ylim([-6 0]); title('OA DDM LV2'); xlabel('Load')
subplot(5,2,8); bar(nanmean(STSWD_summary.brainscoreSummary.DDM_LV3(idx_OA,1:4),1)); ylim([0 10]); title('OA DDM LV3'); xlabel('Load')
subplot(5,2,10); bar(nanmean(STSWD_summary.brainscoreSummary.DDM_LV5(idx_OA,1:4),1)); ylim([0 5]); title('OA DDM LV5'); xlabel('Load')


figure;
bar([nanmean(STSWD_summary.brainscoreSummary.DDM_LV1(idx_YA,5),1), nanmean(STSWD_summary.brainscoreSummary.DDM_LV1(idx_OA,5),1)]); ylim([0 1]); title('YA DDM LV1'); xlabel('Load')

idx_YA = find(idx_YA); idx_YA(STSWD_summary.brainscoreSummary.DDM_LV1(idx_YA,5)==0) = [];
idx_OA = find(idx_OA); idx_OA(STSWD_summary.brainscoreSummary.DDM_LV1(idx_OA,5)==0) = [];

dat{1} = STSWD_summary.brainscoreSummary.DDM_LV1(idx_YA,5); 
dat{2} = STSWD_summary.brainscoreSummary.DDM_LV1(idx_OA,5); 

colorm = [.5 .5 .5];

hold on;
    % if we want each bar to have a different color, loop
    for b = 1:size(dat, 2)
        bar(b, nanmean(dat{b}), 'FaceColor',  colorm(1,:), 'EdgeColor', 'none', 'BarWidth', 0.6);
    end

    addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/B_reliabilityAcrossSessions/D_tools/ploterr/')
    
    % show standard deviation on top
    for b = 1:size(dat, 2)
        h1{b} = ploterr(b, nanmean(dat{b},1), [], nanstd(dat{b},[],1)./sqrt(size(dat{b},1)), 'k.', 'abshhxy', 0);
        set(h1{b}(1), 'marker', 'none'); % remove marker
        set(h1{b}(2), 'LineWidth', 4);
    end

    % label what we're seeing
    % if labels are too long to fit, use the xticklabelrotation with about -30
    % to rotate them so they're readable
    set(gca, 'xtick', [1 2 3 4], 'xticklabel', {'1', '2', '3', '4'}, ...
        'xlim', [0.5 4.5]); ylim([0 1.1])
    ylabel('mean RT'); xlabel('# of targets');

    % if these data are paired, show the differences
    % plot(dat', '.k-', 'linewidth', 0.2, 'markersize', 2);

        % significance star for the difference
        [~, pval] = ttest2(dat{1}, dat{2}); % paired t-test
        % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
        % sigstars on top
        %mysigstar(gca, [indCond+.1 indCond+.9], 1, pval);

        
%% plot intercept-change correlations of behavior

figure;
subplot(4,2,1);
scatter(STSWD_summary.behav.MRIAcc(:,1), STSWD_summary.behav.MRIAcc(:,4)-STSWD_summary.behav.MRIAcc(:,1), 'filled'); xlabel('MRI Acc L1'); ylabel('MRI Acc L4-1')
subplot(4,2,2);
scatter(STSWD_summary.behav.EEGAcc(:,1), STSWD_summary.behav.EEGAcc(:,4)-STSWD_summary.behav.EEGAcc(:,1), 'filled'); xlabel('EEG Acc L1'); ylabel('EEG Acc L4-1')
subplot(4,2,3);
scatter(STSWD_summary.behav.MRIRT(:,1), STSWD_summary.behav.MRIRT(:,4)-STSWD_summary.behav.MRIRT(:,1), 'filled'); xlabel('MRI RT L1'); ylabel('MRI RT L4-1')
subplot(4,2,4);
scatter(STSWD_summary.behav.EEGRT(:,1), STSWD_summary.behav.EEGRT(:,4)-STSWD_summary.behav.EEGRT(:,1), 'filled'); xlabel('EEG RT L1'); ylabel('EEG RT L4-1')
subplot(4,2,5);
scatter(STSWD_summary.behav.MRIAcc(:,1), STSWD_summary.behav.MRIRT(:,4)-STSWD_summary.behav.MRIRT(:,1), 'filled'); xlabel('MRI Acc L1'); ylabel('MRI RT L4-1')
subplot(4,2,6);
scatter(STSWD_summary.behav.EEGAcc(:,1), STSWD_summary.behav.EEGRT(:,4)-STSWD_summary.behav.EEGRT(:,1), 'filled'); xlabel('EEG Acc L1'); ylabel('EEG RT L4-1')
subplot(4,2,7);
scatter(STSWD_summary.behav.MRIRT(:,1), STSWD_summary.behav.MRIAcc(:,4)-STSWD_summary.behav.MRIAcc(:,1), 'filled'); xlabel('MRI RT L1'); ylabel('MRI Acc L4-1')
subplot(4,2,8);
scatter(STSWD_summary.behav.EEGRT(:,1), STSWD_summary.behav.EEGAcc(:,4)-STSWD_summary.behav.EEGAcc(:,1), 'filled'); xlabel('EEG RT L1'); ylabel('EEG Acc L4-1')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

% calculate relative change scores

figure;
subplot(4,2,1);
scatter(STSWD_summary.behav.MRIAcc(:,1), (STSWD_summary.behav.MRIAcc(:,4)-STSWD_summary.behav.MRIAcc(:,1))./STSWD_summary.behav.MRIAcc(:,1), 'filled'); xlabel('MRI Acc L1'); ylabel('MRI Acc L4-1, rel. change')
subplot(4,2,2);
scatter(STSWD_summary.behav.EEGAcc(:,1), (STSWD_summary.behav.EEGAcc(:,4)-STSWD_summary.behav.EEGAcc(:,1))./STSWD_summary.behav.EEGAcc(:,1), 'filled'); xlabel('EEG Acc L1'); ylabel('EEG Acc L4-1, rel. change')
subplot(4,2,3);
scatter(STSWD_summary.behav.MRIRT(:,1), (STSWD_summary.behav.MRIRT(:,4)-STSWD_summary.behav.MRIRT(:,1))./STSWD_summary.behav.MRIRT(:,1), 'filled'); xlabel('MRI RT L1'); ylabel('MRI RT L4-1, rel. change')
subplot(4,2,4);
scatter(STSWD_summary.behav.EEGRT(:,1), (STSWD_summary.behav.EEGRT(:,4)-STSWD_summary.behav.EEGRT(:,1))./STSWD_summary.behav.EEGRT(:,1), 'filled'); xlabel('EEG RT L1'); ylabel('EEG RT L4-1, rel. change')
subplot(4,2,5);
scatter(STSWD_summary.behav.MRIAcc(:,1), (STSWD_summary.behav.MRIRT(:,4)-STSWD_summary.behav.MRIRT(:,1))./STSWD_summary.behav.MRIRT(:,1), 'filled'); xlabel('MRI Acc L1'); ylabel('MRI RT L4-1, rel. change')
subplot(4,2,6);
scatter(STSWD_summary.behav.EEGAcc(:,1), (STSWD_summary.behav.EEGRT(:,4)-STSWD_summary.behav.EEGRT(:,1))./STSWD_summary.behav.EEGRT(:,1), 'filled'); xlabel('EEG Acc L1'); ylabel('EEG RT L4-1, rel. change')
subplot(4,2,7);
scatter(STSWD_summary.behav.MRIRT(:,1), (STSWD_summary.behav.MRIAcc(:,4)-STSWD_summary.behav.MRIAcc(:,1))./STSWD_summary.behav.MRIAcc(:,1), 'filled'); xlabel('MRI RT L1'); ylabel('MRI Acc L4-1, rel. change')
subplot(4,2,8);
scatter(STSWD_summary.behav.EEGRT(:,1), (STSWD_summary.behav.EEGAcc(:,4)-STSWD_summary.behav.EEGAcc(:,1))./STSWD_summary.behav.EEGAcc(:,1), 'filled'); xlabel('EEG RT L1'); ylabel('EEG Acc L4-1, rel. change')
set(findall(gcf,'-property','FontSize'),'FontSize',18)


figure;
subplot(4,2,1);
scatter(HDDM_summary_v12_a12_t2.nondecisionMRI(:,1), (HDDM_summary_v12_a12_t2.nondecisionMRI(:,4)-HDDM_summary_v12_a12_t2.nondecisionMRI(:,1))./HDDM_summary_v12_a12_t2.nondecisionMRI(:,1), 'filled'); xlabel('MRI NDT L1'); ylabel('MRI NDT L4-1, rel. change')
subplot(4,2,2);
scatter(HDDM_summary_v12_a12_t2.nondecisionEEG(:,1), (HDDM_summary_v12_a12_t2.nondecisionEEG(:,4)-HDDM_summary_v12_a12_t2.nondecisionEEG(:,1))./HDDM_summary_v12_a12_t2.nondecisionMRI(:,1), 'filled'); xlabel('EEG NDT L1'); ylabel('EEG NDT L4-1, rel. change')
subplot(4,2,3);
scatter(HDDM_summary_v12_a12_t2.driftMRI(:,1), (HDDM_summary_v12_a12_t2.driftMRI(:,4)-HDDM_summary_v12_a12_t2.driftMRI(:,1))./HDDM_summary_v12_a12_t2.driftMRI(:,1), 'filled'); xlabel('MRI Drift L1'); ylabel('MRI Drift L4-1, rel. change')
subplot(4,2,4);
scatter(HDDM_summary_v12_a12_t2.driftEEG(:,1), (HDDM_summary_v12_a12_t2.driftEEG(:,4)-HDDM_summary_v12_a12_t2.driftEEG(:,1))./HDDM_summary_v12_a12_t2.driftEEG(:,1), 'filled'); xlabel('EEG Drift L1'); ylabel('EEG Drift L4-1, rel. change')
subplot(4,2,5);
scatter(HDDM_summary_v12_a12_t2.nondecisionMRI(:,1), (HDDM_summary_v12_a12_t2.driftMRI(:,4)-HDDM_summary_v12_a12_t2.driftMRI(:,1))./HDDM_summary_v12_a12_t2.driftMRI(:,1), 'filled'); xlabel('MRI NDT L1'); ylabel('MRI Drift L4-1, rel. change')
subplot(4,2,6);
scatter(HDDM_summary_v12_a12_t2.driftMRI(:,1), (HDDM_summary_v12_a12_t2.nondecisionMRI(:,4)-HDDM_summary_v12_a12_t2.nondecisionMRI(:,1))./HDDM_summary_v12_a12_t2.nondecisionMRI(:,1), 'filled'); xlabel('MRI Drift L1'); ylabel('MRI NDT L4-1, rel. change')
subplot(4,2,7);
scatter(HDDM_summary_v12_a12_t2.nondecisionEEG(:,1), (HDDM_summary_v12_a12_t2.driftEEG(:,4)-HDDM_summary_v12_a12_t2.driftEEG(:,1))./HDDM_summary_v12_a12_t2.driftEEG(:,1), 'filled'); xlabel('EEG NDT L1'); ylabel('EEG Drift L4-1, rel. change')
subplot(4,2,8);
scatter(HDDM_summary_v12_a12_t2.driftEEG(:,1), (HDDM_summary_v12_a12_t2.nondecisionEEG(:,4)-HDDM_summary_v12_a12_t2.nondecisionEEG(:,1))./HDDM_summary_v12_a12_t2.nondecisionEEG(:,1), 'filled'); xlabel('EEG Drift L1'); ylabel('EEG NDT L4-1, rel. change')
set(findall(gcf,'-property','FontSize'),'FontSize',18)


figure;
subplot(4,2,1);
scatter(HDDM_summary_v12_a12_t2.nondecisionMRI(:,1), (STSWD_summary.behav.MRIAcc(:,4)-STSWD_summary.behav.MRIAcc(:,1))./STSWD_summary.behav.MRIAcc(:,1), 'filled'); xlabel('MRI NDT L1'); ylabel('MRI Acc L4-1, rel. change')
subplot(4,2,2);
subplot(4,2,3);
scatter(HDDM_summary_v12_a12_t2.driftMRI(:,1), (STSWD_summary.behav.MRIRT(:,4)-STSWD_summary.behav.MRIRT(:,1))./STSWD_summary.behav.MRIRT(:,1), 'filled'); xlabel('MRI Drift L1'); ylabel('MRI RT L4-1, rel. change')
subplot(4,2,4);
subplot(4,2,5);
scatter(HDDM_summary_v12_a12_t2.nondecisionMRI(:,1), (STSWD_summary.behav.MRIRT(:,4)-STSWD_summary.behav.MRIRT(:,1))./STSWD_summary.behav.MRIRT(:,1), 'filled'); xlabel('MRI NDT L1'); ylabel('MRI RT L4-1, rel. change')
subplot(4,2,6);
scatter(HDDM_summary_v12_a12_t2.driftMRI(:,1), (STSWD_summary.behav.MRIAcc(:,4)-STSWD_summary.behav.MRIAcc(:,1))./STSWD_summary.behav.MRIAcc(:,1), 'filled'); xlabel('MRI Drift L1'); ylabel('MRI Acc L4-1, rel. change')
subplot(4,2,7);
scatter(HDDM_summary_v12_a12_t2.driftMRI(:,1), STSWD_summary.behav.MRIAcc(:,1), 'filled'); xlabel('MRI Drift L1'); ylabel('MRI Acc L1')
subplot(4,2,8);
scatter((HDDM_summary_v12_a12_t2.nondecisionMRI(:,4)-HDDM_summary_v12_a12_t2.nondecisionMRI(:,1))./HDDM_summary_v12_a12_t2.nondecisionMRI(:,1), (STSWD_summary.behav.MRIRT(:,4)-STSWD_summary.behav.MRIRT(:,1))./STSWD_summary.behav.MRIRT(:,1), 'filled'); xlabel('MRI NDT L4-L1'); ylabel('MRI RT L4-1, rel. change')

set(findall(gcf,'-property','FontSize'),'FontSize',18)
