% plot brainscore correlations

%% get brainscore data (meancent0): task positive network

pn.data = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B_PLS/B_data/SD_STSWD_task_v1/';
load([pn.data, 'meancentPLS_STSWD_task_N97_byAge_3mm_1000P1000B_BfMRIresult'])

IDs_PLS = cellfun(@(x) x(18:end), subj_name, 'UniformOutput', false);

groupsizes=result.num_subj_lst;
conditions=lv_evt_list;

conds = {'dim1'; 'dim2'; 'dim3'; 'dim4'};

condData = []; uData = [];
for indGroup = 1:2
    if indGroup == 1
        relevantEntries = 1:groupsizes(1)*numel(conds);
    elseif indGroup == 2
        relevantEntries = groupsizes(1)*numel(conds)+1:...
             groupsizes(1)*numel(conds)+groupsizes(2)*numel(conds);
    end
    for indCond = 1:4
        targetEntries = relevantEntries(conditions(relevantEntries)==indCond);        
        condData{indGroup}(indCond,:) = result.vsc(targetEntries,1);
        % ATTENTION: INVERT VALUES FOR INTERPRETABILITY
        uData{indGroup}(indCond,:) = -1.*result.usc(targetEntries,1);
    end
%     uData{indGroup} = uData{indGroup}-repmat(mean(mean(uData{indGroup})),size(uData{indGroup},1),size(uData{indGroup},2)); % subtract mean
%     condData{indGroup} = condData{indGroup}-repmat(mean(mean(condData{indGroup})),size(condData{indGroup},1),size(condData{indGroup},2)); % subtract mean
end

meancent0_uData = uData;

%% load meancent1: bottom-up component

pn.data = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B_PLS/B_data/SD_STSWD_task_v1/';
load([pn.data, 'meancent1PLS_STSWD_task_N97_byAge_3mm_1000P1000B_BfMRIresult.mat'])

IDs_PLS = cellfun(@(x) x(18:end), subj_name, 'UniformOutput', false);

groupsizes=result.num_subj_lst;
conditions=lv_evt_list;

conds = {'dim1'; 'dim2'; 'dim3'; 'dim4'};

condData = []; uData = [];
for indGroup = 1:2
    if indGroup == 1
        relevantEntries = 1:groupsizes(1)*numel(conds);
    elseif indGroup == 2
        relevantEntries = groupsizes(1)*numel(conds)+1:...
             groupsizes(1)*numel(conds)+groupsizes(2)*numel(conds);
    end
    for indCond = 1:4
        targetEntries = relevantEntries(conditions(relevantEntries)==indCond);        
        condData{indGroup}(indCond,:) = result.vsc(targetEntries,1);
        uData{indGroup}(indCond,:) = result.usc(targetEntries,1);
    end
end

meancent1_uData = uData;

%% correlation between brainscores (uncentered)

X = meancent0_uData{1}'; % dimord:{group}cond*subject
Y = meancent1_uData{1}';

[r, p] = corrcoef([X,Y]);

figure; imagesc(r, [-1 1]); colorbar; title('Correlations meancent0, meancent')

figure;
scatter(meancent0_uData{1}(1,:), meancent1_uData{1}(1,:))

%% load SD brainscores (behavioral) [LVs 1,2,3,5]

pn.data = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/D_PLS_Dim/B_data/SD_STSWD_byCond/';
load([pn.data, 'behavPLS_STSWD_N97_SDBOLD_DDM1234_234min1_3mm_1000P1000B_BfMRIresult.mat'])

IDs_PLS = cellfun(@(x) x(4:end), subj_name, 'UniformOutput', false);

groupsizes=result.num_subj_lst;
conditions=lv_evt_list;

conds = {'dim1'; 'dim2'; 'dim3'; 'dim4'; 'dim234min1'};

LVs = [1,2,3,5];
for indLV = 1:numel(LVs)
    condData = []; uData = [];
    for indGroup = 1:2
        if indGroup == 1
            relevantEntries = 1:groupsizes(1)*numel(conds);
        elseif indGroup == 2
            relevantEntries = groupsizes(1)*numel(conds)+1:...
                 groupsizes(1)*numel(conds)+groupsizes(2)*numel(conds);
        end
        for indCond = 1:5
            targetEntries = relevantEntries(conditions(relevantEntries)==indCond);        
            SD_uData{indLV,indGroup}(indCond,:) = result.usc(targetEntries,LVs(indLV));
        end
    end
end

% invert scores for LV 2 + 5
SD_uData{2,1} = -1.*SD_uData{2,1};
SD_uData{2,2} = -1.*SD_uData{2,2};
SD_uData{4,1} = -1.*SD_uData{4,1};
SD_uData{4,2} = -1.*SD_uData{4,2};

%% get SD brainscore PCA dimensionality

pn.data = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/D_PLS_Dim/B_data/SD_STSWD_byCond/';
load([pn.data, 'SDBOLD_BehavPLS_N93_SDDim1234_logAlphaPow1234_3mm_1000P1000B_BfMRIresult.mat'])

IDs_PLS_Dim = cellfun(@(x) x(4:end), subj_name, 'UniformOutput', false);

groupsizes=result.num_subj_lst;
conditions=lv_evt_list;

conds = {'dim1'; 'dim2'; 'dim3'; 'dim4'};

condData = []; uData = [];
for indGroup = 1:2
    if indGroup == 1
        relevantEntries = 1:groupsizes(1)*numel(conds);
    elseif indGroup == 2
        relevantEntries = groupsizes(1)*numel(conds)+1:...
             groupsizes(1)*numel(conds)+groupsizes(2)*numel(conds);
    end
    for indCond = 1:4
        targetEntries = relevantEntries(conditions(relevantEntries)==indCond);        
        SDDim_uData{indGroup}(indCond,:) = result.usc(targetEntries,1);
    end
end

% invert component
SDDim_uData{1} = -1.*SDDim_uData{1};
SDDim_uData{2} = -1.*SDDim_uData{2};

%% intercorrelations between uncentered brain scores

X1 = meancent0_uData{1}'; % dimord:{group}cond*subject
X2 = meancent1_uData{1}';
X3 = SD_uData{1,1}';
X4 = SD_uData{2,1}';
X5 = SD_uData{3,1}';
X6 = SD_uData{4,1}';

[r, p] = corrcoef([X1,X2,X3,X4,X5,X6]);

r(p>.05) = 0;
figure; imagesc(r, [-1 1]); colorbar; title('Correlations between LVs: YA')


X1 = meancent0_uData{2}'; % dimord:{group}cond*subject
X2 = meancent1_uData{2}';
X3 = SD_uData{1,2}';
X4 = SD_uData{2,2}';
X5 = SD_uData{3,2}';
X6 = SD_uData{4,2}';

[r, p] = corrcoef([X1,X2,X3,X4,X5,X6]);

r(p>.05) = 0;
figure; imagesc(r, [-1 1]); colorbar; title('Correlations between LVs: OA')


%% correlations including Dimensionality component

idxYA = cell2mat(cellfun(@str2num, IDs_PLS(:), 'UniformOutput', false))<2000;
idxOA = cell2mat(cellfun(@str2num, IDs_PLS(:), 'UniformOutput', false))>2000;

idxCommon = ismember(IDs_PLS, IDs_PLS_Dim);
idxCommon_YA = idxCommon(idxYA);
idxCommon_OA = idxCommon(idxOA);

X1 = meancent0_uData{1}(:,idxCommon_YA)'; % dimord:{group}cond*subject
X2 = meancent1_uData{1}(:,idxCommon_YA)';
X3 = SD_uData{1,1}(:,idxCommon_YA)';
X4 = SD_uData{2,1}(:,idxCommon_YA)';
X5 = SD_uData{3,1}(:,idxCommon_YA)';
X6 = SD_uData{4,1}(:,idxCommon_YA)';
X7 = SDDim_uData{1}';

[r, p] = corrcoef([X1,X2,X3,X4,X5,X6,X7]);

r(p>.05) = 0;
h = figure('units','normalized','position',[.1 .1 .7 .4]);
subplot(1,2,1); imagesc(r, [-1 1]); colorbar; title('Correlations between LVs: YA')

X1 = meancent0_uData{2}(:,idxCommon_OA)'; % dimord:{group}cond*subject
X2 = meancent1_uData{2}(:,idxCommon_OA)';
X3 = SD_uData{1,2}(:,idxCommon_OA)';
X4 = SD_uData{2,2}(:,idxCommon_OA)';
X5 = SD_uData{3,2}(:,idxCommon_OA)';
X6 = SD_uData{4,2}(:,idxCommon_OA)';
X7 = SDDim_uData{2}';

[r, p] = corrcoef([X1,X2,X3,X4,X5,X6,X7]);

r(p>.05) = 0;
subplot(1,2,2); imagesc(r, [-1 1]); colorbar; title('Correlations between LVs: OA')
set(findall(gcf,'-property','FontSize'),'FontSize',18)
pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/C_figures/';
figureName = 'B_brainscoreCorrelations';
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

%% load updated mean & SD BOLD LVs (meancentered)

% mean BOLD

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B3_PLS_mean_v3/B_data/A_STSWD_meancent/meancentPLS_STSWD_task_N97_byAge_3mm_1000P1000B_BfMRIresult.mat')

IDs_PLS = cellfun(@(x) x(18:end), subj_name, 'UniformOutput', false);

groupsizes=result.num_subj_lst;
conditions=lv_evt_list;

conds = {'dim1'; 'dim2'; 'dim3'; 'dim4'};

condData = []; uData = [];
for indGroup = 1:2
    if indGroup == 1
        relevantEntries = 1:groupsizes(1)*numel(conds);
    elseif indGroup == 2
        relevantEntries = groupsizes(1)*numel(conds)+1:...
             groupsizes(1)*numel(conds)+groupsizes(2)*numel(conds);
    end
    for indCond = 1:4
        targetEntries = relevantEntries(conditions(relevantEntries)==indCond);        
        condData{indGroup}(indCond,:) = result.vsc(targetEntries,1);
        uData{indGroup}(indCond,:) = result.usc(targetEntries,1);
    end
end

meancent0_v2_uData = uData;

% SDBOLD

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/D_PLS_Dim/B_data/SD_STSWD_byCond_v2/meancentPLS_STSWD_SD_N97_3mm_1000P1000B_byAge_BfMRIresult.mat')

IDs_PLS = cellfun(@(x) x(18:end), subj_name, 'UniformOutput', false);

groupsizes=result.num_subj_lst;
conditions=lv_evt_list;

conds = {'dim1'; 'dim2'; 'dim3'; 'dim4'};

condData = []; uData = [];
for indGroup = 1:2
    if indGroup == 1
        relevantEntries = 1:groupsizes(1)*numel(conds);
    elseif indGroup == 2
        relevantEntries = groupsizes(1)*numel(conds)+1:...
             groupsizes(1)*numel(conds)+groupsizes(2)*numel(conds);
    end
    for indCond = 1:4
        targetEntries = relevantEntries(conditions(relevantEntries)==indCond);        
        condData{indGroup}(indCond,:) = result.vsc(targetEntries,1);
        % ATTENTION: brainscores inverted
        uData{indGroup}(indCond,:) = -1.*result.usc(targetEntries,1);
    end
end

meancent_SD_v2_uData = uData;

%% correlate mean & SD BOLD

figure;
scatter(meancent_SD_v2_uData{1}(1,:), meancent0_v2_uData{1}(1,:))
scatter(meancent_SD_v2_uData{1}(4,:), meancent0_v2_uData{1}(4,:))
scatter(meancent_SD_v2_uData{2}(1,:), meancent0_v2_uData{2}(1,:))
scatter(meancent_SD_v2_uData{2}(4,:), meancent0_v2_uData{2}(4,:))

scatter(meancent_SD_v2_uData{1}(4,:)-meancent_SD_v2_uData{1}(1,:), meancent0_v2_uData{1}(4,:)-meancent0_v2_uData{1}(1,:))
scatter(meancent_SD_v2_uData{2}(4,:)-meancent_SD_v2_uData{2}(1,:), meancent0_v2_uData{2}(4,:)-meancent0_v2_uData{2}(1,:))

%% load -4123 contrast

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/D_PLS_Dim/B_data/SD_STSWD_byCond_v2/meancentContrast-4123PLS_STSWD_SD_N97_3mm_1000P1000B_byAge_BfMRIresult.mat')

IDs_PLS = cellfun(@(x) x(18:end), subj_name, 'UniformOutput', false);

groupsizes=result.num_subj_lst;
conditions=lv_evt_list;
conditions(conditions==5) = []; % No idea why there is a condition 5 here!

conds = {'dim1'; 'dim2'; 'dim3'; 'dim4'};

condData = []; uData = [];
for indGroup = 1:2
    if indGroup == 1
        relevantEntries = 1:groupsizes(1)*numel(conds);
    elseif indGroup == 2
        relevantEntries = groupsizes(1)*numel(conds)+1:...
             groupsizes(1)*numel(conds)+groupsizes(2)*numel(conds);
    end
    for indCond = 1:4
        targetEntries = relevantEntries(conditions(relevantEntries)==indCond);        
        condData{indGroup}(indCond,:) = result.vsc(targetEntries,1);
        uData{indGroup}(indCond,:) = result.usc(targetEntries,1);
    end
end

contrast_min4123 = uData;

%% place PLS LVs into structure for export

brainscoreSummary.IDs = IDs_PLS;
brainscoreSummary.meanTask = [meancent1_uData{1}'; meancent1_uData{2}'];
brainscoreSummary.DDM_LV1 = [SD_uData{1,1}'; SD_uData{1,2}'];
brainscoreSummary.DDM_LV2 = [SD_uData{2,1}'; SD_uData{2,2}'];
brainscoreSummary.DDM_LV3 = [SD_uData{3,1}'; SD_uData{3,2}'];
brainscoreSummary.DDM_LV5 = [SD_uData{4,1}'; SD_uData{4,2}'];
brainscoreSummary.meanTaskv2 = [meancent0_v2_uData{1}'; meancent0_v2_uData{2}'];
brainscoreSummary.SDTaskv2 = [meancent_SD_v2_uData{1}'; meancent_SD_v2_uData{2}'];
brainscoreSummary.contrast_min4123 = [contrast_min4123{1}'; contrast_min4123{2}'];

save(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/B_data/B_brainscoreSummary.mat'], 'brainscoreSummary')

%% WIP plots

figure; hold on;
for indCond = 1:4
    scatter(brainscoreSummary.contrast_min4123(:,indCond), brainscoreSummary.SDTaskv2(:,indCond), 'filled')
end

figure
    scatter(brainscoreSummary.contrast_min4123(:,4)-brainscoreSummary.contrast_min4123(:,1), ...
        brainscoreSummary.SDTaskv2(:,4)-brainscoreSummary.SDTaskv2(:,1), 'filled')
